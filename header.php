<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$container = get_theme_mod( 'understrap_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php do_action( 'wp_body_open' ); ?>

<!--PRELOADER-->
<div class="preloader">
	<div class="status"></div>
</div>
<!--/PRELOADER-->

<div class="site" id="page">

	<!-- ******************* The Navbar Area ******************* -->
	<div id="wrapper-navbar" class="fixed-top" itemscope itemtype="http://schema.org/WebSite">

		<a class="skip-link sr-only sr-only-focusable" href="#content"><?php esc_html_e( 'Skip to content', 'understrap' ); ?></a>

		<nav class="navbar navbar-expand-md navbar-dark">

		<?php if ( 'container' == $container ) : ?>
			<div class="container">
		<?php endif; ?>

					<!-- Your site title as branding in the menu -->
					<?php if ( ! has_custom_logo() ) { ?>

						<?php if ( is_front_page() && is_home() ) : ?>

							<h1 class="navbar-brand mb-0"><a rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><?php bloginfo( 'name' ); ?></a></h1>

						<?php else : ?>

							<a class="navbar-brand" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><?php bloginfo( 'name' ); ?></a>

						<?php endif; ?>


					<?php } else {
						the_custom_logo();
					} ?><!-- end custom logo -->

				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="<?php esc_attr_e( 'Toggle navigation', 'understrap' ); ?>">
					<span class="navbar-toggler-icon"></span>
				</button>

				<!-- The WordPress Menu goes here -->
				<?php wp_nav_menu(
					array(
						'theme_location'  => 'primary',
						'container_class' => 'collapse navbar-collapse',
						'container_id'    => 'navbarNavDropdown',
						'menu_class'      => 'navbar-nav mr-auto',
						'fallback_cb'     => '',
						'menu_id'         => 'main-menu',
						'depth'           => 2,
						'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
					)
				); ?>

				<!-- Add buttons here -->
				<ul class="btn-wrapper list-inline d-flex">
				<?php
					if( have_rows( 'header_button_one', 'options') ){
						while( have_rows( 'header_button_one' ,'options') ): the_row();
				?>
					<li class="list-inline-item"><a class="btn<?php echo ' '.get_sub_field( 'style' ); ?> btn-navigation<?php if( !get_sub_field( 'show_on_mobile' ) ){ echo ' d-none d-xl-flex'; } ?>" href="<?php if( get_sub_field( 'internal_url' ) ){	the_sub_field( 'internal_url' ); } elseif( get_sub_field( 'external_url' ) ){ the_sub_field( 'external_url' ); } else { echo 'javascript:void(0);'; } ?>"
					<?php if( get_sub_field( 'external_url' ) ){ ?> target="_blank"<?php } ?>><span><?php the_sub_field( 'label' ); ?></span></a></li>
				<?php
						endwhile;
					}
				?>
				<?php
					if( have_rows( 'header_button_two', 'options') ){
						while( have_rows( 'header_button_two' ,'options') ): the_row();
				?>
					<li class="list-inline-item"><a class="btn<?php echo ' '.get_sub_field( 'style' ); ?> btn-navigation<?php if( !get_sub_field( 'show_on_mobile' ) ){ echo ' d-none d-xl-flex'; } ?>" href="<?php if( get_sub_field( 'internal_url' ) ){	the_sub_field( 'internal_url' ); } elseif( get_sub_field( 'external_url' ) ){ the_sub_field( 'external_url' ); } else { echo 'javascript:void(0);'; } ?>"
					<?php if( get_sub_field( 'external_url' ) ){ ?> target="_blank"<?php } ?>><span><?php the_sub_field( 'label' ); ?></span></a></li>
				<?php
						endwhile;
					}
				?>
				</ul>
				
			<?php if ( 'container' == $container ) : ?>
			</div><!-- .container -->
			<?php endif; ?>
			
		</nav><!-- .site-navigation -->
		
		<section class="site-under-navigation">
		<?php if ( 'container' == $container ) : ?>
			<div class="container">
		<?php endif; ?>
				<div class="row">
					<div class="col-7">
						<div class="event-meta"><span class="dates"><?php the_field( 'header_date', 'options' ); ?></span> | <span class="location"><?php the_field( 'header_location', 'options' ); ?></span></div>
					</div>
					<div class="col-5">
						<div class="social-media">
							<?php if( have_rows( 'header_social_media', 'options' ) ){ ?>
							<ul class="list-inline">
								<?php while( have_rows( 'header_social_media', 'options' ) ): the_row(); ?>
								<li class="list-inline-item">
									<a <?php if( get_sub_field( 'internal_url' ) ){ ?>href="<?php the_sub_field( 'internal_url' ); ?>" <?php } elseif( 'external_url' ){ ?>href="<?php the_sub_field( 'external_url' ); ?>" target="_blank" <?php } else { echo 'href="javascript:void(0);"'; } ?>><?php the_sub_field( 'label' ); ?></a>
								</li>
								<?php endwhile; ?>
							</ul>
							<?php } ?>
						</div>
					</div>
				</div>
		<?php if ( 'container' == $container ) : ?>
			</div>
		<?php endif; ?>
		</section><!-- .site-under-navigation -->
	</div><!-- #wrapper-navbar end -->
