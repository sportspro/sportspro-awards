<?php
/**
 * Template Name: Events Page
 *
 * Template for displaying a category/categories of speakers.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<div class="wrapper section-standard section-page section-padding" id="page-wrapper">

	<div class="container" id="content">

		<div class="row">

			<div class="col-md content-area" id="primary">

				<main class="site-main" id="main" role="main">

					<?php while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'loop-templates/content', 'events' );  ?>


					<?php endwhile; // end of the loop. ?>

				</main><!-- #main -->

			</div><!-- #primary -->

		</div>

	</div><!-- #content -->

</div><!-- #page-wrapper -->

<?php get_footer();
