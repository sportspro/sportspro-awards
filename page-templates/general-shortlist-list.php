<?php
/**
 * Template Name: General Shortlist List Template
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

while ( have_posts() ) :
	the_post();
	get_template_part( 'loop-templates/content', 'general-shortlist-list' );
endwhile;

get_footer();
