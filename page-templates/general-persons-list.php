<?php
/**
 * Template Name: General Persons List Template
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

while ( have_posts() ) :
	the_post();
	get_template_part( 'loop-templates/content', 'general-persons-list' );
endwhile;

get_footer();
