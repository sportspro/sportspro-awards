<?php
/**
 * Template Name: Registration Page Template
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

while ( have_posts() ) :
	the_post();
	get_template_part( 'loop-templates/content', 'general-brand-registration' );
endwhile;
?>
<script src="https://js.tito.io/v1" async=""></script>
<?php
get_footer();
