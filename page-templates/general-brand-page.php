<?php
/**
 * Template Name: General Brand Page Template
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

while ( have_posts() ) :
	the_post();
	get_template_part( 'loop-templates/content', 'general-brand-page' );
endwhile;

get_footer();
