/****************************************
 *
 *	Theme:	SportsPro Awards
 *	Author:	Simione Talanoa
	Email:	stalanoa@henleymediagroup.com
 *	Ver:	1.00
 *
 * This theme requires the latest version of JQuery to be declared.
 *
 */
(function ($) {
  $(document).ready(function () {
    var offset = $('#wrapper-navbar').height(); //$('#full-width-page-wrapper').css( 'margin-top', offset );

    $('#page-wrapper').css('margin-top', offset);
    /* If home page is rendered, apply transparent navbar */

    $('.home #wrapper-navbar').addClass('bg-transparent');
    /* For fixed top site navigation menu */

    window.addEventListener("scroll", function () {
      if (true) {
        if (window.scrollY > 90) {
          $('.home #wrapper-navbar').removeClass('bg-transparent');
          $('.home #wrapper-navbar').addClass('bg-navbar');
        } else {
          $('.home #wrapper-navbar').addClass('bg-transparent');
          $('.home #wrapper-navbar').removeClass('bg-navbar');
        }
      }
    }, false);
    /* For venbox popups */

    $('.venobox').venobox();
    $('.venobox-categories').venobox({
      framewidth: '420px'
    });
    $('.venobox-judges').venobox({
      framewidth: '640px'
    });
    $('.venobox-companies').venobox({
      framewidth: '640px'
    });
    /* Example carousel */

    $('.partners-carousel').slick({
      autoplay: true,
      slidesToShow: 6
    });
  });
  $(window).resize(function () {
    var offset = $('#wrapper-navbar').height(); //$('#full-width-page-wrapper').css( 'margin-top', offset );

    $('#page-wrapper').css('margin-top', offset);
  });
  $(window).load(function () {
    // will first fade out the loading animation
    $('.status').fadeOut(); // will fade out the whole DIV that covers the website.

    $('.preloader').delay(100).fadeOut('slow');
    $('body').css('overflow-y', 'visible'); // Add a delay before scrolling window

    setTimeout(function () {
      /* code to handle navigation issues with anchors because of loading of HTML */
      var url = window.location.href; // Returns full URL	

      var hash = url.substring(url.indexOf('#') + 1);
      if (hash != url && hash != "") if ($('#' + hash).length) window.scrollTo(0, $('#' + hash).offset().top - $('#wrapper-navbar').height() - 20);
    }, 500);
  });
  $(window).scroll(function () {
    if ($(window).scrollTop() + $(window).height() >= $(document).height() - $('#wrapper-footer').height()) {
      $('.event-countdown').removeClass('fixed-bottom');
    } else {
      if ($('.event-countdown.collapse.show').length) $('.event-countdown').addClass('fixed-bottom');else $('.event-countdown').removeClass('fixed-bottom');
    }
  });
})(jQuery);