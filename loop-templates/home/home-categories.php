<section class="categories section-standard"
<?php
		while ( have_rows( 'categories_section_options' )): the_row();
			echo 'style="';
			if ( get_sub_field( 'background_colour' ) ){
				echo 'background:';
				the_sub_field( 'background_colour' );
				echo ';';
			} else {
				echo 'background: #021F37;';
			}
			if ( get_sub_field( 'colour' ) ){
				echo 'color:';
				the_sub_field( 'colour' );
				echo ';';
			} else {
				echo 'color: #FFF;';
			}
			echo '"';
		endwhile;
?>
>
	<div class="container-fluid p-0">
		<div class="row align-items-center">
			<div class="col-12 col-md-6">
				<?php 
					$categories = get_field( 'categories_selector' );
					if ( $categories ){
				?>
				<div class="grid">
					<div class="row no-gutters">
						<?php
							foreach( $categories as $category ):
						?>
						<div class="col-6 col-md-3">
							<a class="venobox-categories" data-vbtype="inline" href="#category-<?php echo $category->ID ;?>">
							<div class="grid-item">
								<div class="img-wrapper">
									<img class="img-fluid" src="<?php echo the_field( 'category_image', $category->ID ); ?>" alt="<?php echo get_the_title( $category->ID ); ?>" title="<?php echo get_the_title( $category->ID ); ?>" />
								</div>
								<?php
								/*
								<div class="content-wrapper">
									<h3><?php echo get_the_title( $category->ID ); ?></h3>
								</div>
								*/
								?>
							</div>
							</a>
							<!-- Start Inline Popup -->
							<div class="modal" id="category-<?php echo $category->ID; ?>">
								<div class="category modal-wrapper">
									<div class="top">
										<figure class="figure"> 
											<img class="figure-img img-fluid" src="<?php the_field( 'category_image', $category->ID); ?>" alt="<?php echo get_the_title( $category->ID ); ?>" />
											<figcaption class="figure-caption"><?php echo get_the_title( $category->ID ); ?></figcaption>
										</figure>
									</div>
									<div class="bottom">
										<div class="description">
											<?php the_field( 'category_description', $category->ID ); ?>
										</div>
									<?php
										if( have_rows( 'category_cta', $category->ID ) ){
									?>
										<ul class="btn-wrapper list-inline text-center">
											<?php
												while( have_rows( 'category_cta', $category->ID ) ): the_row();
											?>
											<li class="list-inline-item">
											<a class="btn btn-md <?php the_sub_field( 'style' ); ?>" <?php if( get_sub_field( 'internal_url' ) ){ ?>href="<?php the_sub_field( 'internal_url' ); the_sub_field( 'anchor' ); ?>" <?php } elseif( 'external_url' ){ ?>href="<?php the_sub_field( 'external_url' ); the_sub_field( 'anchor' ); ?>" target="_blank" <?php } else { echo 'href="javascript:void(0);"'; } ?>><?php the_sub_field( 'label' ); ?></a>
											</li>
											<?php
												endwhile;
											?>
										</ul>
									<?php
										}
									?>
									</div>
								</div>
							</div>
						</div>
						<?php
							endforeach;
						?>
						<?php
							if( get_field( 'categories_button_mode' ) ){
						?>
						<div class="col-12 col-md-3">
							<?php
								if( have_rows( 'categories_buttons' ) ){
									while( have_rows( 'categories_buttons' ) ): the_row();
							?>
							<a <?php if( get_sub_field( 'internal_url' ) ){ ?>href="<?php the_sub_field( 'internal_url' ); ?>" <?php } elseif( 'external_url' ){ ?>href="<?php the_sub_field( 'external_url' ); ?>" target="_blank" <?php } else { echo 'href="javascript:void(0);"'; } ?>>
							<div class="grid-item d-flex align-items-center justify-content-center">
								<div class="content-wrapper">
									<h2><?php the_sub_field( 'label' ); ?></h2>
								</div>
							</div>
							</a>
							<?php
									endwhile;
								}
							?>
						</div>
						<?php
							}
						?>
						<div class="col">
							<div class="grid-item d-flex align-items-center justify-content-center"  style="background-color: #e9d8b5;">
								<div class="content-wrapper">
									<h2>Click the icons to find out more about each category</h2>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php
					}
				?>
			</div>
			<div class="col-12 col-md-6">
				<h2 class="title"><?php the_field( 'categories_heading' ); ?></h2>
				<div class="content-wrapper">
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas odio turpis, posuere id iaculis eget, viverra sit amet ex. Suspendisse sapien ex, gravida vel pulvinar eu, euismod at felis. Sed.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas odio turpis, posuere id iaculis eget, viverra sit amet ex. Suspendisse sapien ex, gravida vel pulvinar eu, euismod at felis. Sed.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas odio turpis, posuere id iaculis eget, viverra sit amet ex. Suspendisse sapien ex, gravida vel pulvinar eu, euismod at felis. Sed.</p>
				</div>
				<div class="button-wrapper">
					<?php
						if( !get_field( 'categories_button_mode' ) && have_rows( 'categories_buttons' ) ){
					?>
					<ul class="btn-wrapper list-inline">
						<?php
						while( have_rows( 'categories_buttons' ) ): the_row();
						?>
						<li class="list-inline-item">
						<a class="btn btn-lg <?php the_sub_field( 'style' ); ?>" <?php if( get_sub_field( 'internal_url' ) ){ ?>href="<?php the_sub_field( 'internal_url' ); ?>" <?php } elseif( 'external_url' ){ ?>href="<?php the_sub_field( 'external_url' ); ?>" target="_blank" <?php } else { echo 'href="javascript:void(0);"'; } ?>><?php the_sub_field( 'label' ); ?></a>
						</li>
						<?php
						endwhile;
						?>
					</ul>
					<?php
						}
					?>
				</div>
			</div>
		</div>

		<?php
		/*
		<h2 class="title"><?php the_field( 'categories_heading' ); ?></h2>
		<?php 
			$categories = get_field( 'categories_selector' );
			if ( $categories ){
		?>
		<div class="grid">
			<div class="row no-gutters justify-content-center">
				<?php
					foreach( $categories as $category ):
				?>
				<div class="col-12 col-md-3">
					<a class="venobox-categories" data-vbtype="inline" href="#category-<?php echo $category->ID ;?>">
					<div class="grid-item">
						<div class="img-wrapper">
							<img class="icon" src="<?php echo the_field( 'category_image', $category->ID ); ?>" alt="<?php echo get_the_title( $category->ID ); ?>" title="<?php echo get_the_title( $category->ID ); ?>" />
						</div>
						<div class="content-wrapper">
							<h3><?php echo get_the_title( $category->ID ); ?></h3>
						</div>
					</div>
					</a>
					<!-- Start Inline Popup -->
					<div class="modal" id="category-<?php echo $category->ID; ?>">
						<div class="category modal-wrapper">
							<div class="top">
								<figure class="figure"> 
									<img class="figure-img img-fluid" src="<?php the_field( 'category_image', $category->ID); ?>" alt="<?php echo get_the_title( $category->ID ); ?>" />
									<figcaption class="figure-caption"><?php echo get_the_title( $category->ID ); ?></figcaption>
								</figure>
							</div>
							<div class="bottom">
								<div class="description">
									<?php the_field( 'category_description', $category->ID ); ?>
								</div>
							<?php
								if( have_rows( 'category_cta', $category->ID ) ){
							?>
								<ul class="btn-wrapper list-inline text-center">
									<?php
										while( have_rows( 'category_cta', $category->ID ) ): the_row();
									?>
									<li class="list-inline-item">
									<a class="btn btn-md <?php the_sub_field( 'style' ); ?>" <?php if( get_sub_field( 'internal_url' ) ){ ?>href="<?php the_sub_field( 'internal_url' ); the_sub_field( 'anchor' ); ?>" <?php } elseif( 'external_url' ){ ?>href="<?php the_sub_field( 'external_url' ); the_sub_field( 'anchor' ); ?>" target="_blank" <?php } else { echo 'href="javascript:void(0);"'; } ?>><?php the_sub_field( 'label' ); ?></a>
									</li>
									<?php
										endwhile;
									?>
								</ul>
							<?php
								}
							?>
							</div>
						</div>
					</div>
					
				</div>
				<?php
					endforeach;
				?>
				<?php
					if( get_field( 'categories_button_mode' ) ){
				?>
				<div class="col-12 col-md-3">
					<?php
						if( have_rows( 'categories_buttons' ) ){
							while( have_rows( 'categories_buttons' ) ): the_row();
					?>
					<a <?php if( get_sub_field( 'internal_url' ) ){ ?>href="<?php the_sub_field( 'internal_url' ); ?>" <?php } elseif( 'external_url' ){ ?>href="<?php the_sub_field( 'external_url' ); ?>" target="_blank" <?php } else { echo 'href="javascript:void(0);"'; } ?>>
					<div class="grid-item d-flex align-items-center justify-content-center">
						<div class="content-wrapper">
							<h2><?php the_sub_field( 'label' ); ?></h2>
						</div>
					</div>
					</a>
					<?php
							endwhile;
						}
					?>
				</div>
				<?php
					}
				?>
			</div>
		</div>
		<?php
			}
		?>
		<?php
			if( !get_field( 'categories_button_mode' ) && have_rows( 'categories_buttons' ) ){
		?>
		<ul class="btn-wrapper list-inline">
			<?php
			while( have_rows( 'categories_buttons' ) ): the_row();
			?>
			<li class="list-inline-item">
			<a class="btn btn-lg <?php the_sub_field( 'style' ); ?>" <?php if( get_sub_field( 'internal_url' ) ){ ?>href="<?php the_sub_field( 'internal_url' ); ?>" <?php } elseif( 'external_url' ){ ?>href="<?php the_sub_field( 'external_url' ); ?>" target="_blank" <?php } else { echo 'href="javascript:void(0);"'; } ?>><?php the_sub_field( 'label' ); ?></a>
			</li>
			<?php
			endwhile;
			?>
		</ul>
		<?php
			}
		*/
		?>
	</div>
</section>