<section class="how-to-enter section-standard section-padding"
<?php
		while ( have_rows( 'how_to_enter_section_options' )): the_row();
			echo 'style="';
			if ( get_sub_field( 'background' ) ){
				echo 'background:';
				the_sub_field( 'background' );
				echo ';';
			} else {
				echo 'background: #021F37;';
			}
			if ( get_sub_field( 'foreground' ) ){
				echo 'color:';
				the_sub_field( 'foreground' );
				echo ';';
			} else {
				echo 'color: #FFF;';
			}
			echo '"';
		endwhile;
?>
>
	<div class="container-fluid">
		<h2 class="title"><?php the_field( 'how_to_enter_heading' ); ?></h2>
		<?php if( have_rows( 'how_to_enter_grid' ) ){ ?>
		<div class="grid">
			<div class="row justify-content-center">
				<?php
					while( have_rows( 'how_to_enter_grid' ) ): the_row();
						if( get_sub_field( 'display' ) == 'vertical' ){
							while( have_rows( 'items' ) ): the_row();
				?>
				<div class="col-12 col-sm-6 col-md-4 col-lg">
					<div class="grid-item">
						<div class="img-wrapper">
							<img class="icon" src="<?php the_sub_field( 'image' ); ?>" alt="<?php the_sub_field( 'title') ; ?>" title="<?php the_sub_field( 'title') ; ?>" />
						</div>
						<div class="content-wrapper">
							<?php the_sub_field( 'body' ); ?>
							<!-- <p>Select the category or categories you wish to enter</p> -->
						</div>
					</div>
				</div>
				<?php
							endwhile;
						}
						else{
							while( have_rows( 'items' ) ): the_row();
				?>
				<div class="col">
					<p>Horizontal implementation not completed</p>
				</div>
				<?php
							endwhile;
						}
					endwhile;
				?>
			</div>
		</div>
		<?php } ?>
		<?php
			if( have_rows( 'how_to_enter_buttons' ) ){
		?>
		<ul class="btn-wrapper list-inline">
			<?php
			while( have_rows( 'how_to_enter_buttons' ) ): the_row();
			?>
			<li class="list-inline-item">
			<a class="btn btn-lg <?php the_sub_field( 'style' ); ?>" <?php if( get_sub_field( 'internal_url' ) ){ ?>href="<?php the_sub_field( 'internal_url' ); ?>" <?php } elseif( 'external_url' ){ ?>href="<?php the_sub_field( 'external_url' ); ?>" target="_blank" <?php } else { echo 'href="javascript:void(0);"'; } ?>><?php the_sub_field( 'label' ); ?></a>
			</li>
			<?php
			endwhile;
			?>
		</ul>
		<?php
			}
		?>
	</div>
</section>