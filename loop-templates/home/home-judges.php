<section class="judges section-standard section-padding"
<?php
		while ( have_rows( 'judges_section_options' )): the_row();
			echo 'style="';
			if ( get_sub_field( 'background_colour' ) ){
				echo 'background:';
				the_sub_field( 'background_colour' );
				echo ';';
			} else {
				echo 'background: #021F37;';
			}
			if ( get_sub_field( 'colour' ) ){
				echo 'color:';
				the_sub_field( 'colour' );
				echo ';';
			} else {
				echo 'color: #FFF;';
			}
			echo '"';
		endwhile;
?>
>
	<div class="container">
		<div class="header-wrapper">
			<h2 class="title"><?php the_field( 'judges_heading' ); ?></h2>
		</div>

		<div class="content-wrapper">
			<?php the_field( 'judges_body' ); ?>
		</div>
	</div>

	<div class="container-fluid p-0">
		<div class="grid persons-list">
			<div class="row justify-content-center no-gutters">

				<?php						
					$categories = get_field( 'judges_list_category' );
					$judges = get_field( 'judges_list' );

					if( $judges ){
						foreach( $judges as $post ):
							setup_postdata( $post );
				?>
				<div class="col-12 col-sm-6 col-md-4 col-lg-3">
					<a class="venobox-judges" data-vbtype="inline" href="#person-<?php echo $post->ID; ?>">

						<div class="card person-card" data-aos="fade-up" data-aos-once="true">
							<div class="card-header">
								<figure class="figure">
									<img class="card-img-top" src="<?php $image=get_field( 'person_image' ); if( $image ){ echo $image[ 'sizes' ][ 'medium' ]; } else { echo 'https://via.placeholder.com/300'; } ?>" alt="<?php the_title(); ?>">
									<figcaption class="figure-caption">
										<h3 class="card-title"><?php the_title(); ?></h3>
										<p class="card-position"><?php the_field( 'person_position' ); ?></p>
										<?php
											$company = get_field( 'person_company' );
											if( $company) {
										?>
										<p class="card-company"><?php echo $company->post_title; ?></p>
										<?php
											}
										?>
									</figcaption>
								</figure>
							</div>
						</div>

					</a> 
				</div>
				<div class="modal" id="person-<?php echo $post->ID; ?>">
					<div class="person modal-wrapper">
						<div class="header">
							<figure class="figure"> 
								<img class="figure-img" src="<?php $image=get_field( 'person_image' ); if( $image ){ echo $image[ 'sizes' ][ 'medium' ]; } else { echo 'https://via.placeholder.com/300'; } ?>" alt="<?php the_title(); ?>">
							</figure>
							<div class="title">
								<h3 class="name"><?php the_title(); ?></h3>
								<p class="position"><?php the_field( 'person_position' ); ?></p>
								<?php
									$company = get_field( 'person_company' );
									if( $company) {
								?>
								<?php /* <p class="company"><?php echo $company->post_title; ?></p> */ ?>
								<div class="logo">
									<img class="img-fluid" src="<?php $image = get_field( 'company_logo', $company->ID ); if( $image ){ echo $image[ 'sizes' ][ 'large' ]; } else { echo 'https://via.placeholder.com/300x150'; } ?>" /></div>
								<?php
									}
								?>
								<!-- <ul class="contacts list-inline"><li class="list-inline-item"><a href="javascript:void(0);" target="_blank"><i class="fa fa-facebook"></i></a></li><li class="list-inline-item"><a href="javascript:void(0);" target="_blank"><i class="fa fa-twitter"></i></a></li><li class="list-inline-item"><a href="javascript:void(0);" target="_blank"><i class="fa fa-linkedin"></i></a></li><li class="list-inline-item"><a href="javascript:void(0);" target="_blank"><i class="fa fa-envelope"></i></a></li></ul> -->
							</div>
						</div>
						<div class="body">
							<div class="description">
								<?php the_field( 'person_biography' ); ?>
							</div>
						</div>
						<div class="footer">
							
						</div>
					</div>
				</div>
				<?php
						endforeach;

					} elseif( $categories ){
						foreach( $categories as $category ):
							$args = array(
								'post_type'			=>	'judge',
								'post_status' 		=>	'publish',
								'posts_per_page' 	=>	 8, 
								'orderby'			=>	'rand',
								'tax_query' => array(
									array(
										'taxonomy' => 'judge_categories',
										'field' => 'slug',
										'terms' => $category->slug ,
									),
								),
							);
							
							$judges = new WP_Query( $args );

							if( $judges->have_posts() ):
								while( $judges->have_posts() ): $judges->the_post();
				?>
				<div class="col-12 col-sm-6 col-md-4 col-lg-3">
					<a class="venobox-judges" data-vbtype="inline" href="#person-<?php echo $judges->ID; ?>">

						<div class="card person-card" data-aos="fade-up" data-aos-once="true">
							<div class="card-header">
								<figure class="figure">
									<img class="card-img-top" src="<?php $image=get_field( 'person_image' ); if( $image ){ echo $image[ 'sizes' ][ 'medium' ]; } else { echo 'https://via.placeholder.com/300'; } ?>" alt="<?php the_title(); ?>">
									<figcaption class="figure-caption">
										<h3 class="card-title"><?php the_title(); ?></h3>
										<p class="card-position"><?php the_field( 'person_position' ); ?></p>
										<?php
											$company = get_field( 'person_company' );
											if( $company) {
										?>
										<p class="card-company"><?php echo $company->post_title; ?></p>
										<?php
											}
										?>
									</figcaption>
								</figure>
							</div>
						</div>

					</a> 
				</div>
				<div class="modal" id="person-<?php echo $judges->ID; ?>">
					<div class="person modal-wrapper">
						<div class="header">
							<figure class="figure"> 
								<img class="figure-img" src="<?php $image=get_field( 'person_image' ); if( $image ){ echo $image[ 'sizes' ][ 'medium' ]; } else { echo 'https://via.placeholder.com/300'; } ?>" alt="<?php the_title(); ?>">
							</figure>
							<div class="title">
								<h3 class="name"><?php the_title(); ?></h3>
								<p class="position"><?php the_field( 'person_position' ); ?></p>
								<?php
									$company = get_field( 'person_company' );
									if( $company) {
								?>
								<?php /* <p class="company"><?php echo $company->post_title; ?></p> */ ?>
								<div class="logo">
									<img class="img-fluid" src="<?php $image = get_field( 'company_logo', $company->ID ); if( $image ){ echo $image[ 'sizes' ][ 'large' ]; } else { echo 'https://via.placeholder.com/300x150'; } ?>" /></div>
								<?php
									}
								?>
								<!-- <ul class="contacts list-inline"><li class="list-inline-item"><a href="javascript:void(0);" target="_blank"><i class="fa fa-facebook"></i></a></li><li class="list-inline-item"><a href="javascript:void(0);" target="_blank"><i class="fa fa-twitter"></i></a></li><li class="list-inline-item"><a href="javascript:void(0);" target="_blank"><i class="fa fa-linkedin"></i></a></li><li class="list-inline-item"><a href="javascript:void(0);" target="_blank"><i class="fa fa-envelope"></i></a></li></ul> -->
							</div>
						</div>
						<div class="body">
							<div class="description">
								<?php the_field( 'person_biography' ); ?>
							</div>
						</div>
						<div class="footer">
							
						</div>
					</div>
				</div>
				<?php
								endwhile;
							endif;
						endforeach;
					} else {


						$args = array(
							'post_type'			=>	'judge',
							'post_status' 		=>	'publish',
							'posts_per_page' 	=>	 8, 
							'orderby'			=>	'rand',
						);
						
						$judges = new WP_Query( $args );
						
						if( $judges->have_posts() ):

							while( $judges->have_posts() ): $judges->the_post();
				?>
				<div class="col-12 col-sm-6 col-md-4 col-lg-3">
					<a class="venobox-judges" data-vbtype="inline" href="#person-<?php echo $person->ID; ?>">
						<div class="card person-card" data-aos="fade-up" data-aos-once="true">
							<div class="card-image">
								<img class="card-img-top" src="<?php if( get_field( 'person_image', $person->ID) ){ the_field('person_image', $person->ID ); } else { echo 'https://via.placeholder.com/300'; } ?>" alt="<?php the_field('person_name', $person->ID); ?>" />
							</div>
							<div class="card-body d-flex flex-column">
								<h3 class="card-title"><?php the_field('person_name', $person->ID); ?></h3>
								<div class="card-text d-flex flex-column  flex-grow-1 justify-content-between">
									<p class="position"><?php the_field( 'person_position', $person->ID ); ?></p>
									<?php
										$companies = get_field( 'person_company', $person->ID );
										if( $companies ):
											foreach( $companies as $company ):
									?>
									<p class="company-logo"><img class="logo" src="<?php the_field('company_logo', $company->ID); ?>" alt="<?php echo get_the_title( $company->ID ); ?>" /></p>
									<?php
											endforeach;
										endif;
									?>
								</div> 
							</div>
						</div>
					</a> 
				</div>
				<div class="modal" id="person-<?php echo $person->ID; ?>">
					<div class="person modal-wrapper">
						<div class="top">
							<figure class="figure"> 
								<img class="figure-img rounded-circle" src="<?php if( get_field( 'person_image', $person->ID) ){ the_field('person_image', $person->ID ); } else { echo 'https://via.placeholder.com/300'; } ?>" alt="<?php the_field('person_name', $person->ID); ?>" />
							</figure>
							<div class="title">
								<h3 class="name"><?php the_field('person_name', $person->ID); ?></h3>
								<p class="position"><?php the_field( 'person_position', $person->ID ); ?></p>
								<?php
										$companies = get_field( 'person_company', $person->ID );
										if( $companies ):
											foreach( $companies as $company ):
								?>
								<p class="company"><?php echo get_the_title( $company->ID ); ?></p>
								<?php
											endforeach;
										endif;
								?>
							</div>
						</div>
						<div class="bottom">
							<div class="description">
								<?php the_field( 'person_biography', $person->ID ); ?>
							</div>
						</div>
					</div>
				</div>

				<?php
							endwhile;	
						endif;
					}					
				?>
				
			</div>
		</div>
	</div>

	<div class="container">
		<?php
			if( have_rows( 'judges_buttons' ) ){
		?>
		<ul class="btn-wrapper list-inline">
			<?php
			while( have_rows( 'judges_buttons' ) ): the_row();
			?>
			<li class="list-inline-item">
			<a class="btn btn-lg <?php the_sub_field( 'style' ); ?>" <?php if( get_sub_field( 'internal_url' ) ){ ?>href="<?php the_sub_field( 'internal_url' ); ?>" <?php } elseif( 'external_url' ){ ?>href="<?php the_sub_field( 'external_url' ); ?>" target="_blank" <?php } else { echo 'href="javascript:void(0);"'; } ?>><?php the_sub_field( 'label' ); ?></a>
			</li>
			<?php
			endwhile;
			?>
		</ul>
		<?php
			}
		?>
	</div>

</section>