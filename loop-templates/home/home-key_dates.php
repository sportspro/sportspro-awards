<section class="key-dates section-standard section-padding"
<?php
		while ( have_rows( 'key_dates_section_options' )): the_row();
			echo 'style="';
			if ( get_sub_field( 'background' ) ){
				echo 'background:';
				the_sub_field( 'background' );
				echo ';';
			} else {
				echo 'background: #021F37;';
			}
			if ( get_sub_field( 'foreground' ) ){
				echo 'color:';
				the_sub_field( 'foreground' );
				echo ';';
			} else {
				echo 'color: #FFF;';
			}
			if ( get_sub_field( 'image' ) ){
				echo 'background-image: url(';
				the_sub_field( 'image' );
				echo ');';
				echo 'background-repeat: no-repeat;background-position: center; background-size: cover;';
			}
			echo '"';
		endwhile;
?>
>
	<div class="container">
		<h2 class="title"><?php the_field( 'key_dates_heading' ); ?></h2>
		<?php
			if( have_rows( 'key_dates_grid' ) ){
		?>
		<div class="grid">
			<div class="row justify-content-center">
			<?php
				while( have_rows( 'key_dates_grid' ) ): the_row();
			?>
				<div class="col-12 col-sm col-lg-3">
					<div class="grid-item">
						<h4 class="milestone-title"><?php the_sub_field( 'title' ); ?></h4>
						<h3 class="milestone-date">
						<?php						
							if( get_sub_field( 'date_from' ) && get_sub_field( 'date_to' ) ){

								$date_from = new DateTime( get_sub_field( 'date_from' ) );
								$date_to = new DateTime( get_sub_field( 'date_to' ) );
								
								echo $date_from->format( "j" );
								echo "-";
								echo $date_to->format( "j M" );
								echo "<br />";
								echo $date_to->format( "Y" );
								
							} else {

								$date_from = new DateTime( get_sub_field( 'date_from' ) );
								echo $date_from->format( "j M" );
								echo "<br />";
								echo $date_from->format( "Y" );
								
							}
						?>
						</h3>
					</div>
				</div>
			<?php
				endwhile;
			?>
			</div>
		</div>
		<?php
			}
		?>
		
		<?php
			if( have_rows( 'key_dates_buttons' ) ) {
		?>
		<ul class="btn-wrapper list-inline">
			<?php
			while( have_rows( 'key_dates_buttons' ) ): the_row();
			?>
			<li class="list-inline-item">
			<a class="btn btn-lg <?php the_sub_field( 'style' ); ?>" <?php if( get_sub_field( 'internal_url' ) ){ ?>href="<?php the_sub_field( 'internal_url' ); ?>" <?php } elseif( the_sub_field( 'external_url' ) ){ ?>href="<?php the_sub_field( 'external_url' ); ?>" target="_blank" <?php } else { echo 'href="javascript:void(0);"'; } ?>><?php the_sub_field( 'label' ); ?></a>
			</li>
			<?php
			endwhile;
			?>
		</ul>
		<?php
			}
		?>
	</div>
</section>