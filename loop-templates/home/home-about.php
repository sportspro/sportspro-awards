<section class="about section-standard section-padding"
<?php
	$section_options = get_field( 'about_section_options' );

	while ( have_rows( 'about_section_options' )): the_row();
		echo 'style="';
		if ( get_sub_field( 'background_colour' ) ){
			echo 'background:';
			the_sub_field( 'background_colour' );
			echo ';';
		} else {
			echo 'background: #021F37;';
		}
		if ( get_sub_field( 'colour' ) ){
			echo 'color:';
			the_sub_field( 'colour' );
			echo ';';
		} else {
			echo 'color: #FFF;';
		}
		echo '"';
	endwhile;
?>
>
	<div class="container">
		<div class="row">
			<?php
				if ( $section_options[ 'mode' ] == 'centre' ) {
			?>
			<div class="col-md-10 mx-auto">
				<h2 class="title"><?php the_field( 'about_heading' ) ?></h2>
				<?php
					if( have_rows( 'about_media' ) ){
						while( have_rows( 'about_media' ) ): the_row();
				?>
				<div class="img-wrapper">
					<?php
					if( have_rows( 'link') ){
						while( have_rows( 'link' ) ): the_row();
					?>
					
					<?php 
						if( get_sub_field( 'internal_url' ) ){ ?>
						<a href="<?php the_sub_field( 'internal_url' ); ?>">
						<?php 
						} elseif ( get_sub_field( 'external_url' ) ) { ?>
						<a href="<?php the_sub_field( 'external_url' ); ?>" target="_blank"<?php the_sub_field( 'attributes' ); ?>>
						<?php
						} else { 
							echo '<a href="javascript:void(0);">'; 
						} ?>
					<?php
						endwhile;
					}
					
					if( get_sub_field( 'image' ) ){ ?>
					<figure>
						<?php if( get_sub_field( 'overlay' ) ){ ?>
						<i><img src="<?php the_sub_field( 'overlay' ) ?>" alt="play"></i>
						<?php } ?>
						<img class="img-fluid" src="<?php the_sub_field( 'image'); ?>" />
					</figure>
					<?php
					}
					
					if( have_rows( 'link') ){
					?>
					</a>
					<?php
					} 
				?>
				</div>
				<?php
						endwhile;
					}
				?>
				<div class="content-wrapper">
					<?php the_field( 'about_body' ); ?>
				</div>
				<?php
					if( have_rows( 'about_buttons' ) ){
				?>
				<div class="button-wrapper">
					<ul class="btn-wrapper list-inline">
						<?php
						while( have_rows( 'about_buttons' ) ): the_row();
						?>
						<li class="list-inline-item">
						<a class="btn btn-md <?php the_sub_field( 'style' ); ?>" <?php if( get_sub_field( 'internal_url' ) ){ ?>href="<?php the_sub_field( 'internal_url' ); ?>" <?php } elseif( 'external_url' ){ ?>href="<?php the_sub_field( 'external_url' ); ?>" target="_blank" <?php } else { echo 'href="javascript:void(0);"'; } ?>><?php the_sub_field( 'label' ); ?></a>
						</li>
						<?php
						endwhile;
						?>
					</ul>
				</div>
				<?php
					}
				?>
			</div>
			<?php
				} elseif ( $section_options[ 'mode' ] == 'right' ) {
			?>
			<div class="col-12 col-md-6">
				<h2 class="title"><?php the_field( 'about_heading' ); ?></h2>
				<div class="content-wrapper">
					<?php the_field( 'about_body' ); ?>
				</div>
				<?php
					if( have_rows( 'about_buttons' ) ){
				?>
				<div class="button-wrapper">
					<ul class="btn-wrapper list-inline">
						<?php
						while( have_rows( 'about_buttons' ) ): the_row();
						?>
						<li class="list-inline-item">
						<a class="btn btn-md <?php the_sub_field( 'style' ); ?>" <?php if( get_sub_field( 'internal_url' ) ){ ?>href="<?php the_sub_field( 'internal_url' ); ?>" <?php } elseif( 'external_url' ){ ?>href="<?php the_sub_field( 'external_url' ); ?>" target="_blank" <?php } else { echo 'href="javascript:void(0);"'; } ?>><?php the_sub_field( 'label' ); ?></a>
						</li>
						<?php
						endwhile;
						?>
					</ul>
				</div>
				<?php
					}
				?>
			</div>
			<div class="col-12 col-md-6">
				<?php
					if( have_rows( 'about_media' ) ){
						while( have_rows( 'about_media' ) ): the_row();
				?>
				<div class="img-wrapper">
					<?php
					if( have_rows( 'link') ){
						while( have_rows( 'link' ) ): the_row();
					?>
					
					<?php 
						if( get_sub_field( 'internal_url' ) ){ ?>
						<a href="<?php the_sub_field( 'internal_url' ); ?>">
						<?php 
						} elseif ( get_sub_field( 'external_url' ) ) { ?>
						<a href="<?php the_sub_field( 'external_url' ); ?>" target="_blank"<?php the_sub_field( 'attributes' ); ?>>
						<?php
						} else { 
							echo '<a href="javascript:void(0);">'; 
						} ?>
					<?php
						endwhile;
					}
					
					if( get_sub_field( 'image' ) ){ ?>
					<figure>
						<?php if( get_sub_field( 'overlay' ) ){ ?>
						<i><img src="<?php the_sub_field( 'overlay' ) ?>" alt="play"></i>
						<?php } ?>
						<img class="img-fluid" src="<?php the_sub_field( 'image'); ?>" />
					</figure>
					<?php
					}
					
					if( have_rows( 'link') ){
					?>
					</a>
					<?php
					} 
				?>
				</div>
				<?php
						endwhile;
					}
				?>
			</div>
			<?php
				} else {
			?>
			<div class="col-12 col-md-6">
				<?php
					if( have_rows( 'about_media' ) ){
						while( have_rows( 'about_media' ) ): the_row();
				?>
				<div class="img-wrapper">
					<?php
					if( have_rows( 'link') ){
						while( have_rows( 'link' ) ): the_row();
					?>
					
					<?php 
						if( get_sub_field( 'internal_url' ) ){ ?>
						<a href="<?php the_sub_field( 'internal_url' ); ?>">
						<?php 
						} elseif ( get_sub_field( 'external_url' ) ) { ?>
						<a href="<?php the_sub_field( 'external_url' ); ?>" target="_blank"<?php the_sub_field( 'attributes' ); ?>>
						<?php
						} else { 
							echo '<a href="javascript:void(0);">'; 
						} ?>
					<?php
						endwhile;
					}
					
					if( get_sub_field( 'image' ) ){ ?>
					<figure>
						<?php if( get_sub_field( 'overlay' ) ){ ?>
						<i><img src="<?php the_sub_field( 'overlay' ) ?>" alt="play"></i>
						<?php } ?>
						<img class="img-fluid" src="<?php the_sub_field( 'image'); ?>" />
					</figure>
					<?php
					}
					
					if( have_rows( 'link') ){
					?>
					</a>
					<?php
					} 
				?>
				</div>
				<?php
						endwhile;
					}
				?>
			</div>
			<div class="col-12 col-md-6">
				<h2 class="title"><?php the_field( 'about_heading' ); ?></h2>
				<div class="content-wrapper">
					<?php the_field( 'about_body' ); ?>
				</div>
				<?php
					if( have_rows( 'about_buttons' ) ){
				?>
				<div class="button-wrapper">
					<ul class="btn-wrapper list-inline">
						<?php
						while( have_rows( 'about_buttons' ) ): the_row();
						?>
						<li class="list-inline-item">
						<a class="btn btn-md <?php the_sub_field( 'style' ); ?>" <?php if( get_sub_field( 'internal_url' ) ){ ?>href="<?php the_sub_field( 'internal_url' ); ?>" <?php } elseif( 'external_url' ){ ?>href="<?php the_sub_field( 'external_url' ); ?>" target="_blank" <?php } else { echo 'href="javascript:void(0);"'; } ?>><?php the_sub_field( 'label' ); ?></a>
						</li>
						<?php
						endwhile;
						?>
					</ul>
				</div>
				<?php
					}
				?>
			</div>
			<?php
				}
			?>

		</div>
	</div>
</section>