<?php
	$options_headings = get_field( 'options_headings', 'options' );
	$options_section = get_field( 'statistics_section_options' );
?>
<section id="statistics" class="statistics section-standard section-padding<?php echo ' '.$options_section[ 'section_padding' ]; ?>" style="<?php if( $options_section[ 'background_colour' ] ){ echo 'background-color: '.$options_section['background_colour'].';'; } if( $options_section[ 'colour' ] ){ echo 'color: '.$options_section['colour'].';'; } ?>">
	<div class="container">
		<div class="section-header">
			<?php if( $options_headings[ 'style' ] == 'icon' ){ ?>
				<?php if( $options_headings[ 'icon' ] ): ?>
			<div class="title-icon">
				<img src="<?php echo $options_headings[ 'icon' ][ 'sizes' ][ 'medium' ]; ?>" alt="icon" />
			</div>
				<?php endif; ?>
			<h3 class="title"><?php the_field( 'statistics_heading' ); ?></h3>
			<?php } elseif( $options_headings[ 'style' ] == 'underline' ){ ?>
			<h3 class="title"><?php the_field( 'statistics_heading' ); ?></h3>
			<hr/>
			<?php } else { ?>
				<?php if( $options_headings[ 'icon' ] ): ?>
			<div class="title-icon">
				<img src="<?php echo $options_headings[ 'icon' ][ 'sizes' ][ 'medium' ]; ?>" alt="icon" />
			</div>
				<?php endif; ?>
			<h3 class="title"><?php the_field( 'statistics_heading' ); ?></h3>
			<hr/>
			<?php } ?>
			<!--
			<div class="title-description">
				<?php /* the_field( 'statistics_body' ); */ ?>
			</div>
			-->
			<!--
			<div class="title-footer">
				<ul class="button-wrapper list-inline">
					<li class="list-inline-item"><a class="btn btn-lg btn-primary" href="">Button One</a></li>
					<li class="list-inline-item"><a class="btn btn-lg btn-secondary" href="">Button Two</a></li>
				</ul>
			</div>
			-->
		</div>
		<div class="section-body">
			<?php
				if( have_rows( 'statistics_data' ) ){
					while( have_rows( 'statistics_data' ) ): the_row();
					
					$show_icons = get_sub_field( 'show_icons' );
			?>
			<div class="row justify-content-center">
					<?php
						while( have_rows( 'items' ) ): the_row();
					?>
				<div class="col-6 col-sm">
					<figure class="figure">
						<?php
							if( $show_icons ){
						?>
						<img class="figure-img" src="<?php $icon=get_sub_field( 'icon' ); if( $icon ){ echo $icon[ 'sizes' ][ 'medium' ]; } else { echo 'https://via.placeholder.com/100x100'; } ?>" width="70">
						<?php
							}
						?>
						<figcaption class="figure-caption">
							<div class="figure-count"><?php the_sub_field( 'number' ); ?></div>
							<div class="figure-heading"><?php the_sub_field( 'title' ); ?></div>
						</figcaption>
					</figure>
				</div>
					<?php
						endwhile;
					?>
			</div>
			<?php
					endwhile;
				}
			?>
		</div>
		<div class="section-footer">
		</div>
	</div>
</section>