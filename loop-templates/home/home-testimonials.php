<?php
	$options_section = get_field( 'testimonials_section_options' );
?>
<section id="testimonials" class="testimonials section-standard<?php echo ' '.$options_section[ 'section_padding' ]; ?>" style="<?php if( $options_section[ 'background_colour' ] ){ echo 'background-color: '.$options_section['background_colour'].';'; } if( $options_section[ 'colour' ] ){ echo 'color: '.$options_section['colour'].';'; } ?>">
	<div class="container-fluid p-0">
		<div class="row no-gutters">
			<?php
				if( have_rows( 'testimonials_grid' ) ){
					while( have_rows( 'testimonials_grid' ) ): the_row();
			?>
			<div class="col-12 col-md-6">
				<div class="testimonials-item" style="background-image: url( <?php if( get_sub_field( 'image_one' ) ){ $image = get_sub_field( 'image_one' ); echo $image[ 'sizes' ][ 'large' ]; } else { echo 'https://via.placeholder.com/800x600'; } ?> );">
				</div>
			</div>
			<div class="col-6 col-md-3">
				<?php while( have_rows( 'testimonial_one' ) ): the_row(); ?>
				<div class="testimonials-item testimonials-item-half testimonial">
					<div class="content-wrapper">
						<blockquote class="blockquote">
							<?php the_sub_field( 'body' ); ?>
							<footer class="blockquote-footer"><?php the_sub_field( 'attribution' ); ?></footer>
						</blockquote>
					</div>
				</div>
				<?php endwhile; ?>
			</div>
			<div class="col-6 col-md-3">
				<div class="testimonials-item testimonials-item-half" style="background-image: url( <?php if( get_sub_field( 'image_two' ) ){ $image = get_sub_field( 'image_two' ); echo $image[ 'sizes' ][ 'large' ]; } else { echo 'https://via.placeholder.com/800x600'; } ?> );">
				</div>
			</div>
			<div class="col-6 col-md-3">
				<?php while( have_rows( 'testimonial_two' ) ): the_row(); ?>
				<div class="testimonials-item testimonials-item-half testimonial">
					<div class="content-wrapper">
						<blockquote class="blockquote">
							<?php the_sub_field( 'body' ); ?>
							<footer class="blockquote-footer"><?php the_sub_field( 'attribution' ); ?></footer>
						</blockquote>
					</div>
				</div>
				<?php endwhile; ?>
			</div>
			<div class="col-12 col-md-6">
				<div class="testimonials-item" style="background-image: url( <?php if( get_sub_field( 'image_three' ) ){ $image = get_sub_field( 'image_three' ); echo $image[ 'sizes' ][ 'large' ]; } else { echo 'https://via.placeholder.com/800x600'; } ?> );">
				</div>
			</div>
			<div class="col-6 col-md-3">
				<?php while( have_rows( 'testimonial_three' ) ): the_row(); ?>
				<div class="testimonials-item testimonials-item-half testimonial">
					<div class="content-wrapper">
						<blockquote class="blockquote">
							<?php the_sub_field( 'body' ); ?>
							<footer class="blockquote-footer"><?php the_sub_field( 'attribution' ); ?></footer>
						</blockquote>
					</div>
				</div>
				<?php endwhile; ?>
			</div>
			<?php
					endwhile;
				}
			?>
		</div>
	</div>
</section>