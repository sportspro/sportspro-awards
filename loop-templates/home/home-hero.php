<?php
	$hero_section_options = get_field( 'hero_section_options' );
?>

<?php 
	if( $hero_section_options[ 'mode' ] == 1 ){
?>
<?php
	if( have_rows( 'hero_slider_type_one' ) ){
		while( have_rows( 'hero_slider_type_one' ) ): the_row();
			$slider_options = get_sub_field( 'slider_options' );
?>
<section class="hero">
	<?php
		if( have_rows( 'slider_items' ) ){
	?>
	<div class="hero-slider" data-slick='<?php if( !$slider_options[ 'default' ] ){ echo $slider_options[ 'advanced' ]; } else { echo '{"arrows": false, "dots": true, "infinite": true, "autoplay": true, "autoplaySpeed": 8000 }'; } ?>'>
		<?php
			while( have_rows( 'slider_items' ) ): the_row();
		?>
		<div class="hero-slider-item">
			<div class="hero-slider-image" style="<?php $image=get_sub_field( 'background_image' ); if( $image ){ ?>background-image: url( <?php echo $image[ 'sizes' ]['large']; ?> );<?php } ?>"></div>

			<?php
				if( have_rows( 'overlay' ) ){
			?>
				<div class="hero-overlay">
				<?php
					while( have_rows( 'overlay' ) ): the_row();
				?>
					<?php
						if( get_sub_field( 'image' ) ){
					?>
					<div class="logo">
						<img class="img-fluid" src="<?php $image= get_sub_field( 'image' ); echo $image[ 'sizes' ]['large'];  ?>">
					</div>
					<?php
						}
						if( get_sub_field( 'surtitle' ) ){
					?>
					<h3 class="meta"><?php the_sub_field( 'surtitle' ); ?></h3>
					<?php
						}
						if( get_sub_field( 'title' ) ){
					?>
					<h2 class="tagline"><?php the_sub_field( 'title' ); ?></h2>
					<?php
						}
						if( get_sub_field( 'subtitle' ) ){
					?>
					<h3 class="meta"><?php the_sub_field( 'subtitle' ); ?></h3>
					<?php
						}
						if( have_rows ( 'buttons' ) ){
					?>
					<div class="button-wrapper">
						<ul class="list-inline">
							<?php
								while( have_rows( 'buttons' ) ): the_row();
							?>
							<li class="list-inline-item">
								<a class="btn btn-md<?php echo ' '.get_sub_field( 'style' ); ?>" <?php if( get_sub_field( 'internal_url' ) ){ ?> href="<?php the_sub_field( 'internal_url'); ?>"<?php } elseif( get_sub_field( 'external_url' ) ){ ?> href="<?php the_sub_field( 'external_url' ); ?>" target="_blank"<?php } else{ ?> href="javascript:void(0);"<?php } ?>><?php the_sub_field( 'label' ); ?></a>
							</li>
							<?php
								endwhile; // 'buttons'
							?>
						</ul>
					</div>	
				<?php					
						}
					endwhile; // 'overlay'
				?>
			</div>
			<?php
				}
			?>
		</div>
		<?php
			endwhile; // 'slider_items'
		?>
	</div>
	<?php
		}
	?>
</section>
<?php
		endwhile;
	}
?>
<?php
	} elseif( $hero_section_options[ 'mode' ] == 2 ){
?>
<?php
	if( have_rows( 'hero_slider_type_two' ) ){
		while( have_rows( 'hero_slider_type_two' ) ): the_row();
			$slider_options = get_sub_field( 'slider_options' );
?>
<section class="hero" style="<?php $image=get_sub_field( 'background_image' ); if( $image ){ ?>background-image: url( <?php echo $image['sizes']['large'] ?>;)<?php } ?>">
	<?php
		if( have_rows( 'slider_items' ) ){
	?>

	<div class="hero-slider" data-slick='<?php if( !$slider_options[ 'default' ] ){ echo $slider_options[ 'advanced' ]; } else { echo '{"arrows": false, "dots": true, "infinite": true, "autoplay": true, "autoplaySpeed": 8000 }'; } ?>'>
		<?php
			while( have_rows( 'slider_items' ) ): the_row();
		?>
		<div class="hero-slider-item">
			<?php
				if( have_rows( 'overlay' ) ){
			?>
				<div class="hero-overlay">
				<?php
					while( have_rows( 'overlay' ) ): the_row();
				?>
					<?php
						if( get_sub_field( 'image' ) ){
					?>
					<div class="logo">
						<img class="img-fluid" src="<?php $image= get_sub_field( 'image' ); echo $image[ 'url' ]; ?>">
					</div>
					<?php
						}
						if( get_sub_field( 'surtitle' ) ){
					?>
					<h3 class="meta"><?php the_sub_field( 'surtitle' ); ?></h3>
					<?php
						}
						if( get_sub_field( 'title' ) ){
					?>
					<h2 class="tagline"><?php the_sub_field( 'title' ); ?></h2>
					<?php
						}
						if( get_sub_field( 'subtitle' ) ){
					?>
					<h3 class="meta"><?php the_sub_field( 'subtitle' ); ?></h3>
					<?php
						}
						if( have_rows ( 'buttons' ) ){
					?>
					<div class="button-wrapper">
						<ul class="list-inline">
							<?php
								while( have_rows( 'buttons' ) ): the_row();
							?>
							<li class="list-inline-item">
								<a class="btn btn-md<?php echo ' '.get_sub_field( 'style' ); ?>" <?php if( get_sub_field( 'internal_url' ) ){ ?> href="<?php the_sub_field( 'internal_url'); ?>"<?php } elseif( get_sub_field( 'external_url' ) ){ ?> href="<?php the_sub_field( 'external_url' ); ?>" target="_blank"<?php } else{ ?> href="javascript:void(0);"<?php } ?>><?php the_sub_field( 'label' ); ?></a>
							</li>
							<?php
								endwhile; // 'buttons'
							?>
						</ul>
					</div>	
				<?php					
						}
					endwhile; // 'overlay'
				?>
				</div>
			<?php
				}
			?>
		</div>
		<?php
			endwhile; // 'slider_items'
		?>
	</div>
	<?php
		}
	?>
</section>
<?php
		endwhile;
	}
?>
<?php
	} elseif( $hero_section_options[ 'mode' ] == 3 ){
?>
<?php
	if( have_rows( 'hero_slider_type_three' ) ){
		while( have_rows( 'hero_slider_type_three' ) ): the_row();
?>
<section class="hero">
	<div class="hero-video">
		<?php
			$background_video = get_sub_field( 'background_video' );
			if( $background_video ){
		?>
		<video muted autoplay loop class="" poster="<?php $image=$background_video[ 'poster' ]; echo $image[ 'sizes' ][ 'large' ]; ?>">
			<source src="<?php echo $background_video[ 'video_url' ]; ?>" type="video/mp4" />
		</video>
		<?php 
			}
			if( have_rows( 'overlay' ) ){
		?>
		<div class="hero-overlay">
		<?php
			while( have_rows( 'overlay' ) ): the_row();
		?>
			<?php
				if( get_sub_field( 'image' ) ){
			?>
			<div class="logo">
				<img class="img-fluid" src="<?php $image= get_sub_field( 'image' ); echo $image[ 'sizes' ]['large'];  ?>">
			</div>
			<?php
				}
				if( get_sub_field( 'surtitle' ) ){
			?>
			<h3 class="meta"><?php the_sub_field( 'surtitle' ); ?></h3>
			<?php
				}
				if( get_sub_field( 'title' ) ){
			?>
			<h2 class="tagline"><?php the_sub_field( 'title' ); ?></h2>
			<?php
				}
				if( get_sub_field( 'subtitle' ) ){
			?>
			<h3 class="meta"><?php the_sub_field( 'subtitle' ); ?></h3>
			<?php
				}
				if( get_sub_field( 'details' ) ){
			?>
			<h4 class="meta"><?php the_sub_field( 'details' ); ?></h4>
			<?php		
				}
				if( have_rows ( 'buttons' ) ){
			?>
			<div class="button-wrapper">
				<ul class="list-inline">
					<?php
						while( have_rows( 'buttons' ) ): the_row();
					?>
					<li class="list-inline-item">
						<a class="btn btn-md<?php echo ' '.get_sub_field( 'style' ); ?>" <?php if( get_sub_field( 'internal_url' ) ){ ?> href="<?php the_sub_field( 'internal_url'); ?>"<?php } elseif( get_sub_field( 'external_url' ) ){ ?> href="<?php the_sub_field( 'external_url' ); ?>" target="_blank"<?php } else{ ?> href="javascript:void(0);"<?php } ?>><?php the_sub_field( 'label' ); ?></a>
					</li>
					<?php
						endwhile;
					?>
				</ul>
			</div>
			<?php					
				}
			?>
		<?php
			endwhile;
		?>
		</div>
		<?php
			}
		?>
	</div>
</section>

<?php
		endwhile;
	}
?>

<?php } else { ?>
<section class="hero">
	<p>Error: No mode selected</p>
</section>	
<?php } ?>