<?php
	$options_headings = get_field( 'options_headings', 'options' );
	$options_section = get_field( 'cta_section_options' );
	$overlay = get_field( 'cta_overlay_2' );
?>
<section class="cta section-standard<?php echo ' '.$options_section[ 'section_padding' ]; ?>" style="<?php if( $options_sections[ 'background_colour' ] ){ echo 'background-color: '.$options_section['background_colour'].';'; } if( $options_sections[ 'colour' ] ){ echo 'color: '.$options_section['colour'].';'; } if( get_field( 'cta_background_image' ) ){ $image = get_field( 'cta_background_image' ); echo 'background-image: url('.$image[ 'sizes' ][ 'large' ].');'; } ?>">
	<div class="container">
		<div class="section-header">
		<?php if( $options_headings[ 'style' ] == 'icon' ){ ?>
				<?php if( $options_headings[ 'icon' ] ): ?>
			<div class="title-icon">
				<img src="<?php echo $options_headings[ 'icon' ][ 'sizes' ][ 'medium' ]; ?>" alt="icon" />
			</div>
				<?php endif; ?>
			<h3 class="title"><?php if( $overlay[ 'title' ] ){ echo $overlay[ 'title' ]; } else { the_title(); } ?></h3>
			<?php } elseif( $options_headings[ 'style' ] == 'underline' ){ ?>
			<h3 class="title"><?php if( $overlay[ 'title' ] ){ echo $overlay[ 'title' ]; } else { the_title(); } ?></h3>
			<hr/>
			<?php } else { ?>
				<?php if( $options_headings[ 'icon' ] ): ?>
			<div class="title-icon">
				<img src="<?php echo $options_headings[ 'icon' ][ 'sizes' ][ 'medium' ]; ?>" alt="icon" />
			</div>
				<?php endif; ?>
			<h3 class="title"><?php if( $overlay[ 'title' ] ){ echo $overlay[ 'title' ]; } else { the_title(); } ?></h3>
			<hr/>
			<?php } ?>
			<?php
				if( $overlay[ 'subtitle' ] ) {
			?>
			<div class="mb-4">
			<?php
					echo $overlay[ 'subtitle' ];
			?>
			</div>
			<?php
				 };
			 ?>
		</div>

		<div class="section-footer">
		<?php
			if( have_rows( 'cta_overlay_2' ) ): while ( have_rows( 'cta_overlay_2' ) ) : the_row(); 
		?>
			<div class="button-wrapper">
				<ul class="list-inline">
			<?php
				if( have_rows( 'buttons' ) ): while ( have_rows( 'buttons' ) ) : the_row();
			?>
					<li class="list-inline-item">
						<a class="btn btn-lg<?php echo ' '.get_sub_field( 'style' ); ?>"<?php if( get_sub_field( 'internal_url' ) ){ ?> href="<?php the_sub_field( 'internal_url'); ?>"<?php } elseif( get_sub_field( 'external_url' ) ){ ?> href="<?php the_sub_field( 'external_url' ); ?>" target="_blank"<?php } else{ ?> href="javascript:void(0);"<?php } ?>><?php echo get_sub_field( 'label' ); ?></a>
					</li>
		<?php
				endwhile; endif;
		?>
				</ul>
			</div>
		<?php
			endwhile; endif;
		?>
		</div>
	</div>
</section>