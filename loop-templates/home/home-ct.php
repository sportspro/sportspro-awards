<?php
/**
 *  Event Countdown: Put this in the footer.
 *
 */
?>

<?php if( have_rows( 'ct_section_options' ) ){ ?>

<?php while ( have_rows( 'ct_section_options' )): the_row(); ?>

<?php if( get_field( 'display' ) == 'true' ){ ?>
<section id="event-countdown" class="event-countdown fixed-bottom collapse show"
<?php
		echo 'style="';
		if ( get_field( 'background_colour' ) ){
			echo 'background:';
			the_field( 'background_colour' );
			echo ';';
		} else {
			echo 'background: #FFFFFF;';
		}
		if ( get_field( 'foreground_colour' ) ){
			echo 'color:';
			the_field( 'foreground_colour' );
			echo ';';
		} else {
			echo 'color: #333333;';
		}
		echo '"';
?>
>
	<style>
		.cloud-city-dash {
		    height: 4.5em !important;
		}
		.cloud-city-digit {
			font-size: 2em !important;
		}
		.cloud-city-dash_title {
			color:<?php the_field( 'foreground_colour' ); ?> !important;
		}
	</style>
	<div class="container position-relative">
		<div class="row justify-content-center align-items-center">
			<div class="dismiss position-absolute" style="right: 5px; top: 5px;z-index:3;"><a data-toggle="collapse" href="#event-countdown" role="button">Dismiss</a></div>
			<div class="col-12 col-xl-auto text-center">
				<?php the_field( 'description' ); ?>
			</div>
			<div class="col-12 col-md-6 col-lg-5">
				<?php echo do_shortcode( '[tminus t="'.get_field( 'countdown_timer' ).'" style="cloud-city" omityears="true" omitmonths="true" omitweeks="true" omitseconds="true"  /]' ); ?>
			</div>
			<div class="col-12 col-md-auto text-center">
				<?php
					if( have_rows( 'buttons' ) ){
						while( have_rows( 'buttons' )): the_row();
				?>
				<a class="btn btn-sm btn-primary" href="
				<?php
					if( get_sub_field( 'internal_url' ) ){
						the_sub_field( 'internal_url' );  
						
					} elseif( get_sub_field( 'external_url' ) ){
						the_sub_field( 'external_url' ); 
					} else { 
						echo 'javascript:void(0);'; 
					} ?>"><?php the_sub_field( 'label' ); ?></a>
				<?php
						endwhile;
					}
				?>
			</div>
		</div>
	</div>
</section>
<?php } ?>
<?php endwhile; ?>
<?php } ?>