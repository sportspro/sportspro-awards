<section class="hero section-standard" <?php if( get_field( 'hero_image' ) && get_field( 'hero_video'  ) == '' ){ ?>style="background: url( <?php the_field( 'hero_image' ); ?> ); background-size: cover;"<?php } ?>>
<?php if( get_field( 'hero_video' ) ){ ?>
	<video class="lazy" playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop" poster="<?php the_field( 'hero_image' ); ?>">
		<source data-src="<?php the_field( 'hero_video' ); ?>" type="video/mp4">
	</video>
<?php } ?>
	<?php if( have_rows( 'hero_overlay' ) ) { ?>
	<div class="hero-overlay">
		<?php while( have_rows( 'hero_overlay' ) ): the_row() ?>
		<div class="logo-wrapper">
			<div class="img-wrapper">
				<img class="logo" src="<?php the_sub_field( 'image' ); ?>" alt="<?php the_sub_field( 'title' ); ?>" title="<?php the_sub_field( 'title' ); ?>" />
			</div>
		</div>
		<h1 class="hero-title"><?php the_sub_field( 'title' ); ?></h1>
		<h2 class="tagline"><?php the_sub_field( 'subtitle' ); ?></h2>
		<h3 class="details"><?php the_sub_field( 'details' ); ?></h3>
		<?php
			if( have_rows( 'buttons' ) ){
		?>
		<ul class="btn-wrapper list-inline">
			<?php while( have_rows( 'buttons' ) ): the_row(); ?>
			<li class="list-inline-item">
				<a class="btn btn-lg <?php the_sub_field( 'style' ); ?>" <?php if( get_sub_field( 'internal_url' ) ){ ?>href="<?php the_sub_field( 'internal_url' ); ?>" <?php } elseif( 'external_url' ){ ?>href="<?php the_sub_field( 'external_url' ); ?>" target="_blank" <?php } else { echo 'href="javascript:void(0);"'; } ?>><?php the_sub_field( 'label' ); ?></a>
			</li>
			<?php endwhile; 
			}
		?>
		</ul>
		<?php endwhile; ?>
	</div>
	<?php } ?>
</section>