<section class="partners section-standard section-padding"
<?php
		while ( have_rows( 'partners_section_options' )): the_row();
			echo 'style="';
			if ( get_sub_field( 'background' ) ){
				echo 'background:';
				the_sub_field( 'background' );
				echo ';';
			} else {
				echo 'background: #021F37';
			}
			if ( get_sub_field( 'foreground' ) ){
				echo 'color:';
				the_sub_field( 'foreground' );
				echo ';';
			} else {
				echo 'color: #FFF;';
			}
			echo '"';
		endwhile;
?>
>
	<div class="container-fluid">
		<h2 class="title">Partners</h2>
		<div class="carousel-wrapper">
			<div class="row justify-content-center">
				<div class="col-11">
				<?php 
					$partners = get_field( 'partners_selector' );
					
					if( $partners ):
				?>
					<div class="partners-carousel">
				<?php foreach( $partners as $partner ): ?>
						<div class="carousel-item">
							<img class="carousel-img img-fluid mx-auto" src="<?php the_field( 'company_logo', $partner->ID );?>" alt="slide 1">
						</div>
				<?php endforeach; ?>
					</div>
				<?php
					endif;
				?>
					
				</div>
			</div>
		</div>
	</div>
</section>