<?php
/**
 * Content general registration partial template.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>
<section class="content-start section-brand section-padding">
	<div class="container">
		<div class="header-wrapper">
			<h1 class="title">
			<?php
				if( get_field( 'registration_heading' ) ){
					the_field( 'registration_heading' );
				} else {
					the_title();
				}					
			?>
			</h1>
			<hr/>
		</div>
		<div class="section-wrapper">
			<section>
				<?php
					if( get_field( 'registration_top_content' ) ) {
				?>
				<div class="content-wrapper">
					<?php the_field( 'registration_top_content' ); ?>
				</div>
				<?php
					}
				?>
			</section>
		</div>
	</div>
	<div class="container-fluid">
		<div class="section-wrapper">
			<section>
			<?php
				if ( have_rows( 'registration_tickets' ) ) {
			?>
				<div class="tickets">
					<div class="row no-gutters justify-content-center">
			<?php
					while ( have_rows( 'registration_tickets' ) ) : the_row();
			?>
						<div class="ticket-item-wrapper <?php
							if(get_sub_field( 'status' ) == 'primary' )
							{
								echo 'primary ';
							}
							elseif( get_sub_field( 'status' ) == 'sold out' )
							{
								echo 'sold-out ';
							}
							else
							{
								echo 'default ';
							}
						
						?>col-12 col-sm-6 col-md-6 col-lg-4">
					<?php
						if ( have_rows( 'superscript' ) )
						{
							while( have_rows( 'superscript' ) ): the_row();
					?>
							<div class="text-center" style="color:<?php if( get_sub_field( 'superscript-colour' ) ) the_sub_field( 'superscript-colour' ); else echo '#333333;';?>">
								<p><?php the_sub_field( 'superscript' ) ?></p>
							</div>
					<?php
							endwhile;
						}
						else {
					?>
							<!--
							<div class="text-center">
								<p>&nbsp;</p>
							</div>
							-->
					<?php
						}
					?>
							<div class="ticket-item d-flex flex-column" <?php if( get_sub_field( 'background_colour' ) ){ ?>style="background-color:<?php the_sub_field( 'background_colour' ); ?>;"<?php } ?>>
								<header class="ticket-heading" <?php if( get_field( 'colour' ) ){ ?>style="color:<?php the_sub_field( 'colour' ); ?>;"<?php } ?>>
									<?php the_sub_field( 'heading' ); ?>
								</header>
								<?php
									if( have_rows( 'price' ) ) {
										while( have_rows( 'price' ) ): the_row();
								?>
								<section class="ticket-pricing">
									<span class="price"><?php the_sub_field( 'amount'); ?></span>
									<span class="info"><?php the_sub_field( 'discount_breakdown' ); ?></span>
								</section>
								<?php
										endwhile;
									}
								?>
								
								<?php
									$benefits_colour = get_sub_field( 'benefits_colour' );
									if ( have_rows( 'benefits') ) {
								?>
								<section class="ticket-benefits d-flex flex-column h-100">
									<ul class="benefits">
									<?php
										while ( have_rows( 'benefits' ) ): the_row();
									?>
										<li
										<?php if ( get_row_index() % 2 == 1 ){ ?>
										style="background-color:rgba( <?php echo implode( ', ', hex2rgb( $benefits_colour ) ); ?>, 0.2);"
										<?php } else { ?>
										style="background-color:inherit;"
										<?php } ?>
										>
										<?php the_sub_field( 'item' ); ?>
										</li>
									<?php
										endwhile;
									?>
									</ul>
								</section>
								<?php		
									}
								?>

								<?php
									if ( have_rows( 'cta' ) ){
								?>
								<footer>
									<?php
										while ( have_rows( 'cta' ) ): the_row();
									?>
									<tito-button event="<?php the_sub_field( 'event' ); ?>" releases="<?php the_sub_field( 'releases' ); ?>"><?php the_sub_field( 'label' ); ?></tito-button>
									<?php
										endwhile;
									?>
								</footer>
								<?php
									}
								?>
							</div>
							
						</div>
			<?php
					endwhile;
			?>
					</div>
				</div>
			<?php
				}
			?>
			</section>
		</div>
	</div>
	<div class="container">
		<div class="section-wrapper">
			<section>
				<?php
					if( get_field( 'registration_terms_and_conditions' ) ) {
				?>
				<div class="content-wrapper">
					<?php the_field( 'registration_terms_and_conditions' ); ?>
				</div>
				<?php
					}
				?>
			</section>
			<section>
				<?php
					if( get_field( 'registration_bottom_content' ) ) {
				?>
				<div class="content-wrapper">
					<?php the_field( 'registration_bottom_content' ); ?>
				</div>
				<?php
					}
				?>
			</section>
		</div>
	
	</div>
</section>