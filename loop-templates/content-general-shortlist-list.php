<?php
/**
 * Content general persons list partial temclate.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>

<section class="content-start section-list section-padding">
	<div class="container">
		<div class="header-wrapper">
			<h1 class="section-title">
			<?php
				if( get_field( 'cl_heading' ) ){
					the_field( 'cl_heading' );
				} else {
					the_title();
				}					
			?>
			</h1>
			<hr/>
		</div>
		<div class="section-wrapper">
			<section>
				<?php if( get_field( 'cl_body' ) ) { ?>
				<div class="content-wrapper">
					<?php the_field( 'cl_body'); ?>
				</div>
				<?php } ?> 
			</section>

			<section>
				<div class="grid categories-list">
					<div class="row justify-content-center">


					<?php
						$selections = get_field( 'cl_selection' );

						if( $selections ):
							foreach( $selections as $selection ):
					?>
						<div class="col-12">
							<h2 class="section-title"><?php echo $selection->post_title; ?></h2>
						</div>

						<?php

							$companies = get_field( 'category_shortlist' , $selection->ID );
							if( $companies ):
								foreach( $companies as $company ):
									$post = $company;
									setup_postdata( $post );
						?>
						<div class="col-6 col-md-3">
							<a class="venobox-companies" data-vbtype="inline" href="#company-<?php echo $post->ID; ?>">
								<div class="card company-card" data-aos="fade-up" data-aos-once="true">
									<div class="card-image">
										<img class="card-img-top" src="<?php if( get_field( 'company_logo' ) ){ the_field( 'company_logo' ); } else { echo 'https://via.placeholder.com/200x100'; } ?>" alt="<?php the_title(); ?>" />
									</div>
									<div class="card-body d-flex flex-column">
										<h3 class="card-title"><?php the_title(); ?></h3>
									</div>
								</div>
							</a> 
						</div>

						<div class="modal" id="company-<?php echo $post->ID; ?>">
							<div class="company modal-wrapper">
								<div class="top">
									<figure class="figure"> 
										<img class="figure-img" src="<?php if( get_field( 'company_logo' ) ){ the_field( 'company_logo' ); } else { echo 'https://via.placeholder.com/200x100'; } ?>" alt="<?php the_title(); ?>" />
									</figure>
									<div class="title">
										<h3 class="name"><?php the_title(); ?></h3>
									</div>
								</div>
								<div class="bottom">
									<div class="description">
										<?php the_field( 'company_description' ); ?>
									</div>
								</div>
							</div>
						</div>

						<?php
								endforeach;
								wp_reset_postdata();
							endif;
						?>
					<?php
							endforeach;
						endif;
					?>
					<?php
					/*
						while( have_rows( 'cl_middle_content' ) ): the_row();
							if( get_sub_field( 'heading' ) ){
					?>
						<div class="col-12">
							<h2 class="section-title"><?php the_sub_field( 'heading' ); ?></h2>				
						</div>
						<?php
							}
						
							$categories = get_sub_field( 'category' );

							if( $categories ){
								foreach( $categories as $category ):
								endforeach;
							} else {
								$args = array(
									'post_type' =>'category',
									'post_status' =>'publish',
									'posts_per_page'  => -1, 
									'orderby' => 'title',
								);
								$persons = get_posts( $args );
								
								if( $persons ):
									foreach( $persons as $person):
						?>
						<div class="col-12 col-sm-6 col-md-4 col-lg-3">
							<a class="venobox-judges" data-vbtype="inline" href="#person-<?php echo $person->ID; ?>">
								<div class="card person-card" data-aos="fade-up" data-aos-once="true">
									<div class="card-image">
										<img class="card-img-top" src="<?php if( get_field( 'person_image', $person->ID) ){ the_field('person_image', $person->ID ); } else { echo 'https://via.claceholder.com/300'; } ?>" alt="<?php the_field('person_name', $person->ID); ?>" />
									</div>
									<div class="card-body d-flex flex-column">
										<h3 class="card-title"><?php the_field('person_name', $person->ID); ?></h3>
										<div class="card-text d-flex flex-column  flex-grow-1 justify-content-between">
											<p class="position"><?php the_field( 'person_position', $person->ID ); ?></p>
											<?php
												$companies = get_field( 'person_company', $person->ID );
												if( $companies ):
													foreach( $companies as $company ):
											?>
											<p class="company-logo"><img class="logo" src="<?php the_field('company_logo', $company->ID); ?>" alt="<?php echo get_the_title( $company->ID ); ?>" /></p>
											<?php
													endforeach;
												endif;
											?>
										</div> 
									</div>
								</div>
							</a> 
						</div>

						<!--
						<div class="modal" id="person-<?php echo $person->ID; ?>">
							<div class="person modal-wrapper">
								<div class="top">
									<figure class="figure"> 
										<img class="figure-img rounded-circle" src="<?php if( get_field( 'person_image', $person->ID) ){ the_field('person_image', $person->ID ); } else { echo 'https://via.claceholder.com/300'; } ?>" alt="<?php the_field('person_name', $person->ID); ?>" />
									</figure>
									<div class="title">
										<h3 class="name"><?php the_field('person_name', $person->ID); ?></h3>
										<p class="position"><?php the_field( 'person_position', $person->ID ); ?></p>
										<?php
												$companies = get_field( 'person_company', $person->ID );
												if( $companies ):
													foreach( $companies as $company ):
										?>
										<p class="company"><?php echo get_the_title( $company->ID ); ?></p>
										<?php
													endforeach;
												endif;
										?>
									</div>
								</div>
								<div class="bottom">
									<div class="description">
										<?php the_field( 'person_biography', $person->ID ); ?>
									</div>
								</div>
							</div>
						</div>
						-->
					<?php
									endforeach;
									
								endif;
							}					
						endwhile;
						*/
					?>
					</div>
				</div>

			</section>

			<section>
				<?php if( get_field( 'cl_bottom_content' ) ) { ?>
				<div class="content-wrapper">
					<?php the_field( 'cl_bottom_content'); ?>
				</div>
				<?php } ?> 
			</section>
		</div>
	</div>
</section>