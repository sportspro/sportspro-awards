<?php
/**
 * Content empty partial template.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$container = get_theme_mod( 'understrap_container_type' );

// Show the selected frontpage content.

	// NOTE: declare slugs of sections to be displayed here
	$sectionSlugs=[
		'hero',
		'statistics',
		'about',
		'judges',
		'categories',
		'partners',
		'how_to_enter',
		'key_dates',
		'testimonials',
		'cta',
		'ct',
	];
	
	$maxSections = 20;
	
	// Determine section order
	$sections=array_fill(0, $maxSections , null);
	foreach ($sectionSlugs as $slug){
		if ($slug != 'hero'){
			while ( have_rows( $slug.'_section_options' ) ): the_row();	// NOTE: the custom field name of the section options must begin with the slug of the section filename
				if ( get_sub_field( 'order' ) >= 1 ) {	// a value of 0 hides the section
					if ( $sections[ get_sub_field('order') ] == '' ){
						$sections[ get_sub_field('order') ] = $slug;
					} else {
						$sections[] = $slug;	// if position is occupied, append the section to the next available index
					}
				}
			endwhile;
		} else {
			$sections[0] = 'hero'; // hero will always be first
		}
	}
	$sections=array_filter($sections);	// remove NULL values if they exist - due to not setting 'order' properly
	
	// Render sections
	foreach ($sections as $section){
		get_template_part( 'loop-templates/home/home', $section );
	}
