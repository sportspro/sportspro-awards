<?php
/**
 * Content general page partial template.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>
<section class="content-start section-page section-padding">
	<div class="container">
		<div class="header-wrapper">
			<h1 class="section-title">
			<?php
				if( get_field( 'page_heading' ) ){
					the_field( 'page_heading' );
				} else {
					the_title();
				}					
			?>
			</h1>
			<hr/>
		</div>
		<div class="section-wrapper">
			<section>
				<?php
					if( get_field( 'page_top_content' ) ) {
				?>
				<div class="content-wrapper">
					<?php the_field( 'page_top_content' ); ?>
				</div>
				<?php
					}
				?>
				<?php
					if( have_rows( 'page_buttons' ) ) {
						while( have_rows( 'page_buttons') ): the_row();
							if( get_sub_field( 'position' ) == 'top'){
								
				?>
				<div class="button-wrapper">
					<ul class="btn-wrapper list-inline">
				<?php
						while( have_rows( 'buttons' ) ): the_row();
				?>
						<li class="list-inline-item">
						<a class="btn btn-md <?php the_sub_field( 'style' ); ?>" <?php if( get_sub_field( 'internal_url' ) ){ ?>href="<?php the_sub_field( 'internal_url' ); ?>" <?php } elseif( 'external_url' ){ ?>href="<?php the_sub_field( 'external_url' ); ?>" target="_blank" <?php } else { echo 'href="javascript:void(0);"'; } ?>><?php the_sub_field( 'label' ); ?></a>
						</li>
				<?php
						endwhile;
				?>
					</ul>
				</div>
				<?php
							}
						endwhile;
					}
				?>
			</section>
			
			<section>
		<?php
				if( have_rows( 'page_middle_content' ) ) {
					while( have_rows( 'page_middle_content' ) ): the_row();
		?>
				<div class="content-wrapper">

					<?php
						if( have_rows( 'content' ) ) {
							while ( have_rows( 'content') ): the_row();
								if( get_sub_field( 'image' ) ){
					?>
									<figure class="figure">
										<img class="figure-img img-fluid" src="<?php the_sub_field( 'image' ); ?>" />
										<figcaption lass="figure-caption"><?php the_sub_field( 'image_caption' ); ?></figcaption>
									</figure>
					<?php
								}
								elseif( get_sub_field( 'embed' ) && get_sub_field( 'body' ) ) {
									the_sub_field( 'body' );
									the_sub_field( 'embed' );
								}
								elseif( get_sub_field( 'embed' ) ) {
									the_sub_field( 'embed' );
								}
								else {
									the_sub_field( 'body' );
								}
							endwhile;
						} else if( have_rows( 'grid' ) ) {
						?>
						<div class="grid">
						<?php
							while( have_rows( 'grid' ) ): the_row();
						?>
							<?php
								if( get_sub_field( 'display' ) == 'horizontal' ){
							?>
									<div class="row justify-content-center">
							<?php
									while( have_rows( 'items' ) ): the_row();
							?>
										<div class="col mb-5">
											<div class="media d-flex align-items-top">
												<figure class="mr-5">
													<img src="<?php the_sub_field( 'image' ); ?>" class="img-fluid"/>
												</figure>
												<div class="media-body">
													<h2><?php the_sub_field( 'heading' ); ?></h2>
													<?php the_sub_field( 'body' ); ?>
												</div>
											</div>
										</div>
							<?php
									endwhile;
							?>
									</div>
							<?php
								}
								if( get_sub_field( 'display' ) == 'vertical' ){
							?>
									<div class="row justify-content-center">
							<?php
									while( have_rows( 'items' ) ): the_row();
							?>
										<div class="col-6 col-sm text-center justify-content-center align-items-center mb-2">
											<div class="item">
												<figure class="figure">
													<img class="figure-img img-fluid" src="<?php the_sub_field( 'image' ); ?>" alt="<?php the_sub_field( 'heading' ); ?>">
													<figcaption class="figure-caption">
														<h2><?php the_sub_field( 'heading' ); ?></h2>
														<?php the_sub_field( 'body' ); ?>
													</figcaption>
												</figure>
											</div>
										</div>
							<?php
									endwhile;
							?>
									
									</div>
							<?php
								}
							?>
						<?php
							endwhile;
						?>
						</div>
						<?php
						} else {
					?>
					<div class="row">
					<?php
							while( have_rows( 'left_content') ): the_row();
					?>
						<div class="col-12 col-md-6">
							<?php
								if( get_sub_field( 'image' ) ){
							?>
									<figure class="figure">
										<img class="figure-img img-fluid" src="<?php the_sub_field( 'image' ); ?>" />
										<figcaption lass="figure-caption"><?php the_sub_field( 'image_caption' ); ?></figcaption>
									</figure>
							<?php
								}
								elseif( get_sub_field( 'embed' ) ) {
									the_sub_field( 'embed' );
								}
								else {
									the_sub_field( 'body' );
								}
							?>
						</div>
					<?php
							endwhile;
							
							while( have_rows( 'right_content' ) ): the_row();
					?>
						<div class="col-12 col-md-6">
							<?php
								if( get_sub_field( 'image' ) ){
							?>
									<figure class="figure">
										<img class="figure-img img-fluid" src="<?php the_sub_field( 'image' ); ?>" />
										<figcaption lass="figure-caption"><?php the_sub_field( 'image_caption' ); ?></figcaption>
									</figure>
							<?php
								}
								elseif( get_sub_field( 'embed' ) ) {
									the_sub_field( 'embed' );
								}
								else {
									the_sub_field( 'body' );
								}
							?>
						</div>
					<?php
							endwhile;
					?>
					</div>
					<?php
						}
					?>
				</div>
		<?php
					endwhile;
				}
		?>
			
			
				<?php
					if( have_rows( 'page_buttons' ) ) {
						while( have_rows( 'page_buttons') ): the_row();
							if( get_sub_field( 'position' ) == 'middle'){
								
				?>
				<div class="button-wrapper">
					<ul class="btn-wrapper list-inline">
				<?php
						while( have_rows( 'buttons' ) ): the_row();
				?>
						<li class="list-inline-item">
						<a class="btn btn-md <?php the_sub_field( 'style' ); ?>" <?php if( get_sub_field( 'internal_url' ) ){ ?>href="<?php the_sub_field( 'internal_url' ); ?>" <?php } elseif( 'external_url' ){ ?>href="<?php the_sub_field( 'external_url' ); ?>" target="_blank" <?php } else { echo 'href="javascript:void(0);"'; } ?>><?php the_sub_field( 'label' ); ?></a>
						</li>
				<?php
						endwhile;
				?>
					</ul>
				</div>
				<?php
							}
						endwhile;
					}
				?>
				
			</section>
			<section>
				<?php
					if( get_field( 'page_bottom_content' ) ) {
				?>
				<div class="content-wrapper">
					<?php the_field( 'page_bottom_content' ); ?>
				</div>
				<?php
					}
				?>
				<?php
					if( have_rows( 'page_buttons' ) ) {
						while( have_rows( 'page_buttons') ): the_row();
							if( get_sub_field( 'position' ) == 'bottom'){
								
				?>
				<div class="button-wrapper">
					<ul class="btn-wrapper list-inline">
				<?php
						while( have_rows( 'buttons' ) ): the_row();
				?>
						<li class="list-inline-item">
						<a class="btn btn-md <?php the_sub_field( 'style' ); ?>" <?php if( get_sub_field( 'internal_url' ) ){ ?>href="<?php the_sub_field( 'internal_url' ); ?>" <?php } elseif( 'external_url' ){ ?>href="<?php the_sub_field( 'external_url' ); ?>" target="_blank" <?php } else { echo 'href="javascript:void(0);"'; } ?>><?php the_sub_field( 'label' ); ?></a>
						</li>
				<?php
						endwhile;
				?>
					</ul>
				</div>
				<?php
							}
						endwhile;
					}
				?>
			</section>
		</div>
	</div>
</section>