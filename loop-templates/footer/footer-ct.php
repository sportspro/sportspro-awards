<?php
/**
 *  Countdown Timer:
 *
 */
?>

<?php
	if( have_rows( 'options_countdown_timers', 'options' ) ){
		while( have_rows( 'options_countdown_timers', 'options') ): the_row();
?>

<?php
			if( get_sub_field( 'conditions' ) == 'enable' ){ //show exclusively on pages selected in 'pages'
				$pages = get_sub_field( 'pages' ); // array of strings?
				foreach( $pages as $page):
					if ( rtrim( $page,'/' ) == home_url( $wp-> request ) ){
?>

<?php

		if( have_rows( 'countdown_timer' ) ){
			while ( have_rows( 'countdown_timer' )): the_row();
?>

<div id="event-countdown" class="event-countdown fixed-bottom collapse show"
<?php
		echo 'style="';
		if ( get_sub_field( 'background_colour' ) ){
			echo 'background:';
			the_sub_field( 'background_colour' );
			echo ';';
		} else {
			echo 'background: #FFFFFF;';
		}
		if ( get_sub_field( 'colour' ) ){
			echo 'color:';
			the_sub_field( 'colour' );
			echo ';';
		} else {
			echo 'color: #333333;';
		}
		echo '"';
?>
>
	<style>
		.cloud-city-dash {
		    height: 4.5em !important;
		}
		.cloud-city-digit {
			font-size: 2em !important;
		}
		.cloud-city-dash_title {
			color:<?php the_sub_field( 'colour' ); ?> !important;
		}
		.dismiss {
			color:<?php the_sub_field( 'colour' ); ?> !important;
		}
	</style>
	<div class="dismiss position-absolute" style="left: 5px; top: 5px;z-index:3;"><a data-toggle="collapse" href="#event-countdown" role="button">Dismiss</a></div>
	<div class="container position-relative">
		<div class="row justify-content-center align-items-center">
			<div class="col-12 col-xl-auto text-center">
				<?php the_sub_field( 'description' ); ?>
			</div>
			<div class="col-12 col-md-6 col-lg-5">
				<?php echo do_shortcode( '[tminus t="'.get_sub_field( 'date_time' ).'" style="cloud-city" omityears="true" omitmonths="true" omitweeks="true" omitseconds="true"  /]' ); ?>
			</div>
			<div class="col-12 col-md-auto text-center">
				<?php
					if( have_rows( 'buttons' ) ){
						while( have_rows( 'buttons' )): the_row();
				?>
				<a class="btn btn-sm <?php the_sub_field( 'style' ); ?>" href="
				<?php
					if( get_sub_field( 'internal_url' ) ){
						the_sub_field( 'internal_url' );  
						
					} elseif( get_sub_field( 'external_url' ) ){
						the_sub_field( 'external_url' ); 
					} else { 
						echo 'javascript:void(0);'; 
					} ?>"><?php the_sub_field( 'label' ); ?></a>
				<?php
						endwhile;
					}
				?>
			</div>
		</div>
	</div>
</div>

<?php 
				endwhile;
			}
?>

<?php						
					}
				endforeach;
			}
			elseif ( get_sub_field( 'conditions' ) == 'disable' ){ //show on all pages except on pages selected in 'pages'
				$pages = get_sub_field( 'pages' ); // array of strings?

				$is_render = TRUE;
				foreach( $pages as $page ):
					if ( rtrim( $page, '/' ) == home_url( $wp-> request ) ){
						$is_render = FALSE;
						break;
					} 
				endforeach;
				if( $is_render ){
?>

<?php

		if( have_rows( 'countdown_timer' ) ){
			while ( have_rows( 'countdown_timer' )): the_row();
?>

<div id="event-countdown" class="event-countdown fixed-bottom collapse show"
<?php
		echo 'style="';
		if ( get_sub_field( 'background_colour' ) ){
			echo 'background:';
			the_sub_field( 'background_colour' );
			echo ';';
		} else {
			echo 'background: #FFFFFF;';
		}
		if ( get_sub_field( 'colour' ) ){
			echo 'color:';
			the_sub_field( 'colour' );
			echo ';';
		} else {
			echo 'color: #333333;';
		}
		echo '"';
?>
>
	<style>
		.cloud-city-dash {
		    height: 4.5em !important;
		}
		.cloud-city-digit {
			font-size: 2em !important;
		}
		.cloud-city-dash_title {
			color:<?php the_sub_field( 'colour' ); ?> !important;
		}
		.dismiss {
			color:<?php the_sub_field( 'colour' ); ?> !important;
		}
	</style>
	<div class="dismiss position-absolute" style="left: 5px; top: 5px;z-index:3;"><a data-toggle="collapse" href="#event-countdown" role="button">Dismiss</a></div>
	<div class="container position-relative">
		<div class="row justify-content-center align-items-center">
			<div class="col-12 col-xl-auto text-center">
				<?php the_sub_field( 'description' ); ?>
			</div>
			<div class="col-12 col-md-6 col-lg-5">
				<?php echo do_shortcode( '[tminus t="'.get_sub_field( 'countdown_timer' ).'" style="cloud-city" omityears="true" omitmonths="true" omitweeks="true" omitseconds="true"  /]' ); ?>
			</div>
			<div class="col-12 col-md-auto text-center">
				<?php
					if( have_rows( 'buttons' ) ){
						while( have_rows( 'buttons' )): the_row();
				?>
				<a class="btn btn-sm <?php the_sub_field( 'style' ); ?>" href="
				<?php
					if( get_sub_field( 'internal_url' ) ){
						the_sub_field( 'internal_url' );  
						
					} elseif( get_sub_field( 'external_url' ) ){
						the_sub_field( 'external_url' ); 
					} else { 
						echo 'javascript:void(0);'; 
					} ?>"><?php the_sub_field( 'label' ); ?></a>
				<?php
						endwhile;
					}
				?>
			</div>
		</div>
	</div>
</div>

<?php 
				endwhile;
			}
?>

<?php
				}
			} elseif( get_sub_field( 'conditions' ) == 'disabled' ){
?>
				<!-- COUNTDOWN TIMER: Disabled -->
<?php
			} else { //show on all pages
?>

<?php

		if( have_rows( 'countdown_timer' ) ){
			while ( have_rows( 'countdown_timer' )): the_row();
?>

<div id="event-countdown" class="event-countdown fixed-bottom collapse show"
<?php
		echo 'style="';
		if ( get_sub_field( 'background_colour' ) ){
			echo 'background:';
			the_sub_field( 'background_colour' );
			echo ';';
		} else {
			echo 'background: #FFFFFF;';
		}
		if ( get_sub_field( 'colour' ) ){
			echo 'color:';
			the_sub_field( 'colour' );
			echo ';';
		} else {
			echo 'color: #333333;';
		}
		echo '"';
?>
>
	<style>
		.cloud-city-dash {
		    height: 4.5em !important;
		}
		.cloud-city-digit {
			font-size: 2em !important;
		}
		.cloud-city-dash_title {
			color:<?php the_sub_field( 'colour' ); ?> !important;
		}
		.dismiss {
			color:<?php the_sub_field( 'colour' ); ?> !important;
		}
	</style>
	<div class="dismiss position-absolute" style="left: 5px; top: 5px;z-index:3;"><a data-toggle="collapse" href="#event-countdown" role="button">Dismiss</a></div>
	<div class="container position-relative">
		<div class="row justify-content-center align-items-center">
			<div class="col-12 col-xl-auto text-center">
				<?php the_sub_field( 'description' ); ?>
			</div>
			<div class="col-12 col-md-6 col-lg-5">
				<?php echo do_shortcode( '[tminus t="'.get_sub_field( 'date_time' ).'" style="cloud-city" omityears="true" omitmonths="true" omitweeks="true" omitseconds="true"  /]' ); ?>
			</div>
			<div class="col-12 col-md-auto text-center">
				<?php
					if( have_rows( 'buttons' ) ){
						while( have_rows( 'buttons' )): the_row();
				?>
				<a class="btn btn-sm <?php the_sub_field( 'style' ); ?>" href="
				<?php
					if( get_sub_field( 'internal_url' ) ){
						the_sub_field( 'internal_url' );  
						
					} elseif( get_sub_field( 'external_url' ) ){
						the_sub_field( 'external_url' ); 
					} else { 
						echo 'javascript:void(0);'; 
					} ?>"><?php the_sub_field( 'label' ); ?></a>
				<?php
						endwhile;
					}
				?>
			</div>
		</div>
	</div>
</div>

<?php 
				endwhile;
			}
?>

<?php
			}
		endwhile;
	}
?>