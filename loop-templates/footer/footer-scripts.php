<?php
/**
 *  Scripts:
 *
 */
?>
<!-- START: SCRIPTS added by website editor -->
<?php
	if( have_rows( 'options_scripts', 'options' ) ){
		while( have_rows( 'options_scripts', 'options') ): the_row();
?>

<?php
			if( get_sub_field( 'conditions' ) == 'enable' ){ //show exclusively on pages selected in 'pages'
				$pages = get_sub_field( 'pages' ); // array of strings?
				foreach( $pages as $page):
					if ( rtrim( $page,'/' ) == home_url( $wp-> request ) ){
?>

						<?php the_sub_field( 'script' ); ?>

<?php						
					}
				endforeach;
			}
			elseif ( get_sub_field( 'conditions' ) == 'disable' ){ //show on all pages except on pages selected in 'pages'
				$pages = get_sub_field( 'pages' ); // array of strings?

				$is_render = TRUE;
				foreach( $pages as $page ):
					if ( rtrim( $page, '/' ) == home_url( $wp-> request ) ){
						$is_render = FALSE;
						break;
					} 
				endforeach;
				if( $is_render ){
?>

						<?php the_sub_field( 'script' ); ?>

<?php
				}
			} elseif( get_sub_field( 'conditions' ) == 'disabled' ) {
?>

				<!-- SCRIPT: Disabled -->

<?php
			} else { //show on all pages
?>

						<?php the_sub_field( 'script' ); ?>

<?php
			}
		endwhile;
	}
?>
<!-- END: SCRIPTS added by website editor -->