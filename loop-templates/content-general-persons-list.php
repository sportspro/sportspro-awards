<?php
/**
 * Content general persons list partial template.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>
<section class="content-start section-list section-padding">
	
	<div class="header-wrapper">
		<div class="container">
			<h1 class="title">
			<?php
				if( get_field( 'pl_heading' ) ){
					the_field( 'pl_heading' );
				} else {
					the_title();
				}					
			?>
			</h1>
			<hr/>
		</div>
	</div>
	<div class="section-wrapper">
		<div class="container">
			<section>
				<?php if( get_field( 'pl_top_content' ) ) { ?>
				<div class="content-wrapper">
					<?php the_field( 'pl_top_content'); ?>
				</div>
				<?php } ?> 
			</section>
		</div>
	</div>
	<div class="section-wrapper">
		<div class="container-fluid p-0">
			<section>

				<div class="grid persons-list">
					<div class="row justify-content-center no-gutters">
					<?php
						while( have_rows( 'pl_middle_content' ) ): the_row();
							if( get_sub_field( 'heading' ) ){
					?>
						<div class="col-12">
							<h2 class="section-title"><?php the_sub_field( 'heading' ); ?></h2>				
						</div>
						<?php
							}
						
							$categories = get_sub_field( 'category' );

							if( $categories ){
								foreach( $categories as $category ):

									$args = array(
										'post_type' =>'judge',
										'post_status' =>'publish',
										'posts_per_page'  => -1, 
										/* 'orderby' => 'title', */
										'tax_query' => array(
       										array (
            									'taxonomy' => 'judge_categories',
            									'field' => 'slug',
            									'terms' => $category->slug,
        									)
    									),
    									'suppress_filters' =>false,
									);
									$persons = get_posts( $args );

									if( $persons ):
										foreach( $persons as $post):
						?>
						<div class="col-12 col-sm-6 col-md-4 col-lg-3">
							<a class="venobox-judges" data-vbtype="inline" href="#person-<?php echo $post->ID; ?>">

								<div class="card person-card" data-aos="fade-up" data-aos-once="true">
									<div class="card-header">
										<figure class="figure">
											<img class="card-img-top" src="<?php $image=get_field( 'person_image' ); if( $image ){ echo $image[ 'sizes' ][ 'medium' ]; } else { echo 'https://via.placeholder.com/300'; } ?>" alt="<?php the_title(); ?>">
											<figcaption class="figure-caption">
												<h3 class="card-title"><?php the_title(); ?></h3>
												<p class="card-position"><?php the_field( 'person_position' ); ?></p>
												<?php
													$company = get_field( 'person_company' );
													if( $company) {
												?>
												<p class="card-company"><?php echo $company->post_title; ?></p>
												<?php
													}
												?>
											</figcaption>
										</figure>
									</div>
								</div>

							</a> 
						</div>
						<div class="modal" id="person-<?php echo $post->ID; ?>">
							<div class="person modal-wrapper">
								<div class="header">
									<figure class="figure"> 
										<img class="figure-img" src="<?php $image=get_field( 'person_image' ); if( $image ){ echo $image[ 'sizes' ][ 'medium' ]; } else { echo 'https://via.placeholder.com/300'; } ?>" alt="<?php the_title(); ?>">
									</figure>
									<div class="title">
										<h3 class="name"><?php the_title(); ?></h3>
										<p class="position"><?php the_field( 'person_position' ); ?></p>
										<?php
											$company = get_field( 'person_company' );
											if( $company) {
										?>
										<?php /* <p class="company"><?php echo $company->post_title; ?></p> */ ?>
										<div class="logo">
											<img class="img-fluid" src="<?php $image = get_field( 'company_logo', $company->ID ); if( $image ){ echo $image[ 'sizes' ][ 'large' ]; } else { echo 'https://via.placeholder.com/300x150'; } ?>" /></div>
										<?php
											}
										?>
										<!-- <ul class="contacts list-inline"><li class="list-inline-item"><a href="javascript:void(0);" target="_blank"><i class="fa fa-facebook"></i></a></li><li class="list-inline-item"><a href="javascript:void(0);" target="_blank"><i class="fa fa-twitter"></i></a></li><li class="list-inline-item"><a href="javascript:void(0);" target="_blank"><i class="fa fa-linkedin"></i></a></li><li class="list-inline-item"><a href="javascript:void(0);" target="_blank"><i class="fa fa-envelope"></i></a></li></ul> -->
									</div>
								</div>
								<div class="body">
									<div class="description">
										<?php the_field( 'person_biography' ); ?>
									</div>
								</div>
								<div class="footer">
									
								</div>
							</div>
						</div>
						<?php

										endforeach;
									endif;
								endforeach;
							} else {
								$args = array(
									'post_type' =>'judge',
									'post_status' =>'publish',
									'posts_per_page'  => -1, 
									'orderby' => 'title',
								);
								$persons = get_posts( $args );
								
								if( $persons ):
									foreach( $persons as $post):
						?>
						<div class="col-12 col-sm-6 col-md-4 col-lg-3">
							<a class="venobox-judges" data-vbtype="inline" href="#person-<?php echo $post->ID; ?>">

								<div class="card person-card" data-aos="fade-up" data-aos-once="true">
									<div class="card-header">
										<figure class="figure">
											<img class="card-img-top" src="<?php $image=get_field( 'person_image' ); if( $image ){ echo $image[ 'sizes' ][ 'medium' ]; } else { echo 'https://via.placeholder.com/300'; } ?>" alt="<?php the_title(); ?>">
											<figcaption class="figure-caption">
												<h3 class="card-title"><?php the_title(); ?></h3>
												<p class="card-position"><?php the_field( 'person_position' ); ?></p>
												<?php
													$company = get_field( 'person_company' );
													if( $company) {
												?>
												<p class="card-company"><?php echo $company->post_title; ?></p>
												<?php
													}
												?>
											</figcaption>
										</figure>
									</div>
								</div>

							</a> 
						</div>
						<div class="modal" id="person-<?php echo $post->ID; ?>">
							<div class="person modal-wrapper">
								<div class="header">
									<figure class="figure"> 
										<img class="figure-img" src="<?php $image=get_field( 'person_image' ); if( $image ){ echo $image[ 'sizes' ][ 'medium' ]; } else { echo 'https://via.placeholder.com/300'; } ?>" alt="<?php the_title(); ?>">
									</figure>
									<div class="title">
										<h3 class="name"><?php the_title(); ?></h3>
										<p class="position"><?php the_field( 'person_position' ); ?></p>
										<?php
											$company = get_field( 'person_company' );
											if( $company) {
										?>
										<?php /* <p class="company"><?php echo $company->post_title; ?></p> */ ?>
										<div class="logo">
											<img class="img-fluid" src="<?php $image = get_field( 'company_logo', $company->ID ); if( $image ){ echo $image[ 'sizes' ][ 'large' ]; } else { echo 'https://via.placeholder.com/300x150'; } ?>" /></div>
										<?php
											}
										?>
										<!-- <ul class="contacts list-inline"><li class="list-inline-item"><a href="javascript:void(0);" target="_blank"><i class="fa fa-facebook"></i></a></li><li class="list-inline-item"><a href="javascript:void(0);" target="_blank"><i class="fa fa-twitter"></i></a></li><li class="list-inline-item"><a href="javascript:void(0);" target="_blank"><i class="fa fa-linkedin"></i></a></li><li class="list-inline-item"><a href="javascript:void(0);" target="_blank"><i class="fa fa-envelope"></i></a></li></ul> -->
									</div>
								</div>
								<div class="body">
									<div class="description">
										<?php the_field( 'person_description' ); ?>
									</div>
								</div>
								<div class="footer">
									
								</div>
							</div>
						</div>
								<?php
									endforeach;
									
								endif;
							}					
						endwhile;
					?>

					</div>
				</div>

			</section>
		</div>

		<div class="container">
			<section>
				<?php if( get_field( 'pl_bottom_content' ) ) { ?>
				<div class="content-wrapper">
					<?php the_field( 'pl_bottom_content'); ?>
				</div>
				<?php } ?> 
			</section>
		</div>
	</div>
</section>