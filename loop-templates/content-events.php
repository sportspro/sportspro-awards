<?php
/**
 * Content empty partial template.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$container = get_theme_mod( 'understrap_container_type' );
$options_headings = get_field( 'options_headings', 'options' );
$main_section = get_field( 'ep_main_section' );
$sub_section = get_field( 'ep_sub_section' );
?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<header class="entry-header section-standard">

	<?php
		if( !$options_headings[ 'display_hero' ] ){
	?>
		<div class="section-header">
			<?php if( $options_headings[ 'style' ] == 'icon' ){ ?>
				<?php if( $options_headings[ 'icon' ] ): ?>
			<div class="title-icon">
				<img src="<?php echo $options_headings[ 'icon' ][ 'sizes' ][ 'medium' ]; ?>" alt="icon" />
			</div>
				<?php endif; ?>
			<h1 class="title"><?php if( $main_section[ 'heading' ] ){ echo $main_section[ 'heading' ]; } else { the_title(); } ?></h1>
			<?php } elseif( $options_headings[ 'style' ] == 'underline' ){ ?>
			<h1 class="title"><?php if( $main_section[ 'heading' ] ){ echo $main_section[ 'heading' ]; } else { the_title(); } ?></h1>
			<hr/>
			<?php } else { ?>
				<?php if( $options_headings[ 'icon' ] ): ?>
			<div class="title-icon">
				<img src="<?php echo $options_headings[ 'icon' ][ 'sizes' ][ 'medium' ]; ?>" alt="icon" />
			</div>
				<?php endif; ?>
			<h1 class="title"><?php if( $main_section[ 'heading' ] ){ echo $main_section[ 'heading' ]; } else { the_title(); } ?></h1>
			<hr/>
			<?php } ?>
		</div>
	<?php 
		}
	?>

	</header><!-- .entry-header -->

	<?php echo get_the_post_thumbnail( $post->ID, 'large' ); ?>

	<div class="entry-content">

	<?php
		if( $main_section[ 'body' ] ){ 
	?>
		<div class="section-body">
			<?php echo $main_section[ 'body' ]; ?>
		</div>
	<?php	
		}
	?>

	<?php
		$buttons = $main_section[ 'buttons' ];
		if( $buttons ){
	?>
		<div class="section-footer">
			<ul class="button-wrapper list-inline">
			<?php
				foreach( $buttons as $button ):
			?>
				<li class="list-inline-item"><a class="btn btn-md<?php echo ' '.$button[ 'style' ]; ?>" <?php if( $button[ 'external_url' ] ){ echo 'target="_blank"'; } ?>href="<?php if( $button[ 'internal_url' ] ){ echo $button[ 'internal_url' ]; } elseif( $button[ 'external_url' ] ){ echo $button[ 'external_url' ]; } else { echo 'javascript:void(0);'; } ?>"><?php echo $button[ 'label' ]; ?></a></li>
			<?php
				endforeach;
			?>
			</ul>
		</div>
	<?php
		}
	?>

		<section class="section-grid events-list section-padding">
			<div class="container">

			<?php
				global $post;
				$args = '';
					if( $post->post_name != 'events'){
					$args = array(
						'post_type'			=>'event',
						'post_status'		=>'publish',
						'posts_per_page'  	=> -1, 
						'orderby' 			=> 'title',
						'order'  			=> 'ASC',
						'tax_query'			=> array(
							array(
								'taxonomy'	=> 'event_categories',
								'field'		=> 'slug',
								'terms'		=> $post->post_name,
							),
						),
						'suppress_filters' =>false,
					);
					} else {
						$args = array(
							'post_type'			=>'event',
							'post_status'		=>'publish',
							'posts_per_page'  	=> -1, 
							'orderby' 			=> 'title',
							'order'  			=> 'ASC',
							'suppress_filters' =>false,
						);
					}
					
					$loop = new WP_Query($args);

					if ( $loop->have_posts() ) 
					{
			?>
				<div class="row justify-content-center">
				<?php
						while ( $loop->have_posts() ) : $loop->the_post();
							setup_postdata( $loop );
							$date = get_field( 'event_date' );
							$button = get_field( 'event_button' );
				?>
					<div class="col-12 col-md-4 col-lg-3">
						<div class="event card">
							<div class="card-header">
								<figure class="figure">
									
									<img class="card-img-top" src="<?php $image = get_field( 'event_logo' ); if( $image ){ echo $image[ 'sizes' ][ 'medium' ]; } else { echo 'https://via.placeholder.com/300x150'; } ?>" alt="<?php the_title(); ?>" />
								</figure>
							</div>
							<div class="card-body">
								<h3 class="card-title"><?php the_title() ?></h3>
								<p class="card-date">
								<?php
									$date_from = DateTime::createFromFormat('d/m/Y', $date[ 'from' ] );
									$date_to = DateTime::createFromFormat('d/m/Y', $date[ 'to' ] );
									
									if( $date_from && $date_to == "" ){
										if( $date[ 'style' ] == 'month-year' ){
											echo $date_from->format( 'M Y' );
										} else {
											echo $date_from->format( 'd M Y' );
										}
									}
									if( $date_from && $date_to ){
										echo $date_from->format( 'd' ).' - '.$date_to->format( 'd M Y' );
									}
								?>
								</p>
								<p class="card-location"><?php the_field( 'event_location' ); ?></p>
								<?php if( $button ){ ?>
								<p class="card-button"><a class="btn btn-md<?php echo ' '.$button[ 'style' ]; ?>" href="<?php if( $button[ 'external_url' ] ){ echo $button[ 'external_url' ]; } else { echo 'javascript:void(0);'; } ?>"><?php echo $button[ 'label' ]; ?></a></p>
								<?php } ?>
							</div>
						</div>
					</div>
				<?php
						endwhile;
						wp_reset_query();
				?>
				</div>
			<?php	
					}
			?>
			
			</div>
		</section>

		<section class="section-standard section-padding-half-bottom">
			<div class="container">
				<?php
					if( $sub_section[ 'heading' ] ){ 
				?>
				<div class="section-header">
					<h2 class="title"><?php if( $sub_section[ 'heading' ] ){ echo $sub_section[ 'heading' ]; } else { echo '&nbsp;'; } ?></h2>
				</div>
				<?php	
					}
				?>
				<?php
					if( $sub_section[ 'body' ] ){ 
				?>
				<div class="section-body">
					<?php echo $sub_section[ 'body' ]; ?>
				</div>
				<?php	
					}
				?>

				<?php
					$buttons = $sub_section[ 'buttons' ];
					if( $buttons ){
				?>
				<div class="section-footer">
					<ul class="button-wrapper list-inline">
					<?php
						foreach( $buttons as $button ):
					?>
						<li class="list-inline-item"><a class="btn btn-md<?php echo ' '.$button[ 'style' ]; ?>" href="<?php if( $button[ 'external_url' ] ){ echo $button[ 'external_url' ]; } else { echo 'javascript:void(0);'; } ?>"><?php echo $button[ 'label' ]; ?></a></li>
					<?php
						endforeach;
					?>
					</ul>
				</div>
				<?php
					}
				?>

			</div>
		</section>

	</div><!-- .entry-content -->

	<footer class="entry-footer">

		<?php edit_post_link( __( 'Edit', 'understrap' ), '<span class="edit-link">', '</span>' ); ?>

	</footer><!-- .entry-footer -->

</article><!-- #post-## -->
