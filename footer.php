<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$container = get_theme_mod( 'understrap_container_type' );
?>

			<?php get_template_part( 'sidebar-templates/sidebar', 'footerfull' ); ?>

			<?php get_template_part( 'loop-templates/footer/footer', 'ct' ); ?>

			<div class="footer wrapper" id="wrapper-footer">

				<div class="<?php echo esc_attr( $container ); ?>">

					<div class="row">

						<div class="col-md-12">
							
							<footer class="site-footer" id="colophon">
								<div class="row">
									<div class="col-12 col-sm-6 col-md-4">
										<div class="footer-panel footer-logo-panel">
											<?php
												$footer_logo = get_field( 'footer_logo', 'options' );
												if( $footer_logo ):
											?>
											
											<a class="footer-logo"<?php if( $footer_logo[ 'external_url' ] ){ ?> href="<?php echo $footer_logo[ 'external_url' ]; ?>" target="_blank"<?php } elseif( $footer_logo[ 'internal_url' ] ){ ?> href="<?php echo $footer_logo[ 'internal_url' ]; ?>"<?php } else { ?> href="<?php echo 'javascript:void(0);'; ?>"<?php } ?>>
												<img src="<?php if( $footer_logo[ 'image' ] ){ echo $footer_logo[ 'image' ][ 'sizes' ][ 'medium' ]; } else { echo 'https://via.placeholder.com/200x100?text=Logo'; } ?>" width="200" alt="" />
											</a>
											<?php
												endif;
											?>
											<?php the_field( 'footer_body', 'options' ); ?>

											<?php
											/*
												if( have_rows( 'footer_logo', 'options' ) ){
													while( have_rows( 'footer_logo', 'options' ) ): the_row();
											?>
											<a class="footer-logo" href="<?php if( get_sub_field( 'internal_url' ) ){ the_sub_field( 'internal_url' ); } elseif ( get_sub_field( 'external_url' ) ) { the_sub_field( 'external_url' ); } else { echo 'javascript:void(0);'; } ?>">
											<img src="<?php the_sub_field( 'image' ); ?>" width="200">
											</a>
											<?php
													endwhile;
												} 
											?>
											<?php the_field( 'footer_body', 'options' ); ?>
											<?php the_field( 'footer_cta', 'options' );
											*/
											?>

										</div>
									</div>
									<div class="col-12 col-sm-6 col-md-4">
										<div class="footer-panel footer-stay-connected">
											<h5><?php the_field( 'footer_column_two_heading', 'options' ); ?></h5>
											<?php
												if( have_rows( 'footer_column_two_items','options' ) ){
													echo '<ul>';
													while( have_rows( 'footer_column_two_items','options' ) ): the_row();
											?>
													<li class=""><a class="" href="<?php if( get_sub_field( 'internal_url' ) ){ the_sub_field( 'internal_url' ); } else { the_sub_field( 'external_url' ); } ?>" target="_blank" aria-current="page"><?php the_sub_field( 'label' ); ?></a></li>
											<?php
													endwhile;
													echo '</ul>';
												}
											?>	
										</div>
									</div>
									<div class="col-12 col-sm-6 col-md">
										<div class="footer-panel footer-etc-links">
											<h5><?php the_field( 'footer_column_three_heading', 'options' ); ?></h5>
											<?php
												if( have_rows( 'footer_column_three_items','options' ) ){
													echo '<ul>';
													while( have_rows( 'footer_column_three_items','options' ) ): the_row();
											?>
													<li class=""><a class="" href="<?php if( get_sub_field( 'internal_url' ) ){ the_sub_field( 'internal_url' ); } else { the_sub_field( 'external_url' ); } ?>" aria-current="page"><?php the_sub_field( 'label' ); ?></a></li>
											<?php
													endwhile;
													echo '</ul>';
												}
											?>
										</div>
									</div>
									<div class="col-12 col-sm-6 col-md">
										<div class="footer-panel social-media-links">
											<h5><?php the_field( 'footer_column_four_heading', 'options' ); ?></h5>
											<?php if( have_rows( 'footer_column_four_items', 'options' ) ){ ?>
											<ul class="list-inline">
												<?php while( have_rows( 'footer_column_four_items', 'options' ) ): the_row(); ?>
												<li class="list-inline-item">
													<a href="<?php if( get_sub_field( 'internal_url' ) ){ the_sub_field( 'internal_url' ); } elseif( get_sub_field( 'external_url' ) ){ the_sub_field( 'external_url' ); } else { echo 'javascript:void(0);'; } ?>" target="_blank">
														<i class="fa fa-lg <?php the_sub_field( 'icon' ); ?>" aria-hidden="true"></i><span></span>
														<span></span>
													</a>
												</li>
												<?php endwhile; ?>
											</ul>
											<?php } ?>
										</div>
									</div>
									<div class="col-12 text-center">
										<p class="copyrights-info"><?php the_field( 'footer_copyright','options' ); ?></p>
									</div>
								</div>
							</footer><!-- #colophon -->

						</div><!--col end -->

					</div><!-- row end -->

				</div><!-- container end -->

			</div><!-- wrapper end -->

		</div><!-- #page we need this extra closing tag here -->

		<?php wp_footer(); ?>
		
		<?php the_field( 'scripts', 'options' ); ?>

	</body>

</html>